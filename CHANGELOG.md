# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Release information follows.  The following sections can be present.

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

----

## Unreleased

## [2.0.0] - 2023-08-22

### Added

- Added a configurable string parser: `trivet::strings::StringParser`
- Added a configurable number parser: `trivet::numbers::NumberParser`
- Added a configurable string encoder: `trivet::strings::StringEncoder`
- Added a configurable comment parser: `trivet::comments::CommentParser`
- Added a struct for quick access to simpler parsing: `trivet::Tools`

### Changed

- Simplified the `Parser` API by removing the need to pass a type.
- Improved performance of the decoder by reading all-at-once.
- Updated the book for 2.0.

### Removed

- Removed deprecated method `Parser.consume_whitespace_and_comments()`.

### Fixed

- Fixed end of file logic in `Parser`.
- Fixed an issue with underfilling the last bytes in `Parser`.
- Many other minor fixes.

## [1.1.1] - 2023-08-10

### Fixed

- Corrected visibility of new comment fields in `Parser`.

### Added

- Added short discussion of performance to the JSON example chapter of the book.

## [1.1.0] - 2023-08-10

### Added

- Added additional unit tests for parsing.
- Added `_ws` methods for functions that consume and match.
- Added the ability to specify comment forms more generally.
- Comments can be treated as whitespace automatically.

### Changed

- Updated all documentation.

### Deprecated

- Deprecated the old `consume_whitespace_and_comments` method since it is less needed and there are methods with shorter names you can use.

## [1.0.3] - 2023-08-09

### Added

- Added tests.

### Changed

- Moved project to a new namespace on GitLab, and updated links.

## [1.0.2] - 2023-08-08

### Added

- Added implementation chapter to the book.
- Several documentation improvements.
- Expanded roadmap.

### Fixed

- A few additional fixes from clippy and trunk.

## [1.0.1] - 2023-08-06

### Fixed

- The `parse_string` method was documented to use the initial delimiter as the final delimiter, but instead used a hard-coded `"`.  Fixed.

## [1.0.0] - 2023-08-06
