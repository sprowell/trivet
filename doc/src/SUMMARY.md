# Trivet

[Cover](cover.md)
[Thanks](thanks.md)

## The Trivet Library

- [Motivation](motivation.md)
- [Building Parsers](building.md)
- [Parsing Primitives](primitives.md)
- [Parsing Numbers](numbers.md)
- [Parsing Strings](string.md)
- [Encoding Strings](encoding.md)
- [JSON Example](json.md)
- [Math Example](math.md)
- [Implementation](implementation.md)
