# Building Parsers

The `trivet` module provides very, *very*, **very** simple parser primitives for building recursive descent parsers.

The root module provides three important structs.

- `Loc` represents a location in the parse
- `Parser` provides parsing services around some stream
- `ParserCore` provides a subset of the facilities of `Parser`

You will seldom (if ever) need to interact with `ParserCore`.  It exists to break a dependency cycle in the library's use graph.

There are two basic parser primitives.

- You can *peek* at upcoming characters
- You can *consume* characters from the stream

A simple application of this is recognizing an integer.  Here is some very simple pseudocode.

```ada
-- The best pseudocode is written in Ada for some reason.
while is_digit(peek) loop
    consume;
end loop;
```

Composites of these primitives are available to siplify things.  For example, the implementation of the above pseudocode might look like the code below.  Peeking at the next character is a low-cost operation.

```rust,ignore
{{#include ../../examples/book_building_parse_integer_1.rs}}
```

That's still a lot of code. `Parser` provides a `take_while` method that handles all of this for us.  Here's a slightly simpler program that parses an integer from standard input.

```rust,ignore
{{#include ../../examples/book_building_parse_integer_2.rs}}
```

Here the `take_while` peeks at and consumes the next character while it is a digit.

If nothing is entered we get `console:1:1: cannot parse integer from empty string`. If we enter a huge number like `99999999999999999999` (that's 20 nines) we get `console:1:1: number too large to fit in target type`. If we enter something like `65fred` then we get `65`, with the parser left pointing at the `f`.

Of course, this probably isn't what we want. Suppose we have a stream of identifiers and numbers, and we want to parse these.  We already know how to parse unsigned integers. Let's see how to parse identifiers.

```rust,ignore
use trivet;

fn parse_identifier(parser: &mut trivet::Parser) -> String {
    let mut result = parser.take_while(|ch| ch.is_alphabetic());
    result += parser.take_while(|ch| ch.is_alphanumeric()).as_str();
    result
}
```

Since this method doesn't generate any errors, we don't have to report any errors and just return the string.

Okay, now let's create a type for our tokens.

```rust,ignore
#[derive(Debug)]
enum Thing {
    Number(u64),
    Id(String),
}
```

We can parse a sequence of tokens and create a vector of `Thing` instances.  The following code does that, and also adds some error handling.  This method always consumes trailing whitespace (which is a good idea) so we add a `_ws` suffix to the name as a visual indicator to users.

```rust,ignore
fn parse_sequence_ws(parser: &mut trivet::Parser) -> trivet::errors::ParseResult<Vec<Thing>> {
    let mut things = Vec::new();
    let _ = parser.consume_ws();
    while !parser.is_at_eof() {
        match parser.peek() {
            ch if ch.is_ascii_digit() => {
                things.push(Thing::Number(parse_unsigned_integer(parser)?));
            }
            ch if ch.is_alphabetic() => {
                things.push(Thing::Id(parse_identifier(parser)));
            }
            ch => {
                return Err(trivet::errors::unexpected_text_error(
                    parser.loc(),
                    "number or identifier",
                    &ch.to_string(),
                ));
            }
        }
        let _ = parser.consume_ws();
    }
    Ok(things)
}
```

The following main function pulls this all together.

```rust,ignore
fn main() {
    let mut parser = trivet::parse_from_stdin();
    match parse_sequence(&mut parser) {
        Err(err) => {
            println!("{}", err);
        }
        Ok(seq) => {
            println!("{:?}", seq);
        }
    }
}
```

That's pretty much it. If it seems like there's a lot of stuff to build in, that's because there *is*.  The `trivet::Parser` is not a parser *generator*, it's a parser *facilitator*.
