# Motivation

**Trivet** is not a parser *generator*, but a parser *facilitator*.  It provides very simple primitives from which, if you are clever, you can build a parser.

The primary design goal of **Trivet** is *no external dependencies*.  It is also nice if the resulting parsers are fast and have good error reporting.  **Trivet** tries to help with that, too.

**Trivet** is a Rust library of tools for writing [recursive descent parsers][].  These are *low level tools*.  **Trivet** is *not* an easy to use library for writing a parser for a complex language, and is not a good choice for languages that need unbounded lookahead... but then what is?

## So Why Trivet?

**Trivet** is *probably* not the parsing library you want.  You don't write a nice description of your language and then generate a parser from that description.  No.  **Trivet** gives you tools to peek at the stream and consume characters from it... and that's pretty much it.

**Trivet** has *no dependencies* other than the Rust standard libraries, and the entire code base is tiny.  Still, **Trivet** deals with the odd encoding on Windows and has been used (as part of other projects) to write some fairly sophisticated parsers.

If all that sounds good, then Welcome!

[recursive descent parsers]: https://en.wikipedia.org/wiki/Recursive_descent_parser

## Contents

The rest of this document provides a short overview of **Trivet**, then a more detailed look at the (small) parsing primitives library, a look at support for number and string parsing, and then larger examples of building complete parsers.  Finally there are notes on the implementation of **Trivet**.

Code samples found in this book are available in the distribution's `examples` folder and are named starting with `book_[chapter]`.
