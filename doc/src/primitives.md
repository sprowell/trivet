# Parsing Primitives

This chapter walks through the parsing primitives of **Trivet**.

## Making a New Parser

Build a new parser with the `Parser::new` method.  You need to specify two things:

- A name for the parsing source.  This would ideally be a file name, but it might also be something like `"<console>"` to indicate that standard input is being parsed.
- A source of characters to parse, given by an instance of `trivet::decoder::Decode`.

There are convenience functions for creating files from different sources.  You will probably always want to use one of these to start things, and not fool with direct use of `Decode`.

|Function            |Use                                                  |
|--------------------|-----------------------------------------------------|
|`parse_from_bytes`  |Create a parser for an `&[u8]`                       |
|`parse_from_string` |Create a parser for a `&str`                         |
|`parse_from_stdin`  |Create a parser for standard input                   |
|`parse_from_path`   |Create a parser for a file specified by a `&PathBuf` |

## Peeking and Consuming

The primary actions are *peeking* at the stream, which allows you to look ahead at upcoming bytes in the stream, and *consuming*, which discards bytes from the stream.  That's basically it.

Note that peeking and consuming work on *characters*.  The input stream is decoded by the `trivet::decode::Decode` struct, and provides UTF-8 characters to the rest of the parser, and supports reading both UTF-16 and UTF-8.

|Method                                               |Use                                                   |
|-----------------------------------------------------|------------------------------------------------------|
|`peek() -> ParseResult<Option<char>>`                |Peek (look ahead) at the next character in the stream |
|`consume()`                                          |Discard the next character from the stream            |
|`is_at_eof() -> bool`                                |Return true if the stream is exhausted                |
|`peek_and_consume(ch: char) -> ParseResult<bool>`    |Peek and optionally consume (see below)               |
|`peek_and_consume_ws(ch: char) -> ParseResult<bool>` |Peek and optionally consume (see below)               |

You will probably use `peek` and `consume` quite a bit to deal with single characters, but these are not ideal for larger things like keywords.

Often if you peek and match a specific character, you will then want to consume it.  For example, you might have code like this.

```rust,ignore
if parser.peek() == '"' {
    parser.consume();
    // Do whatever else needs to be done.
}
```

This can be simplified with `peek_and_consume(ch: char) -> bool`.  This method checks to see if the next character is `ch` and, iff so, consumes it and then returns `true`.  We can replace the above with the following.

```rust,ignore
if parser.peek_and_consume('"') {
    // Do whatever else needs to be done.
}
```

Often once you have matched something like a closing brace, you then want to consume any trailing whitespace.  The `peek_and_consume_ws(ch: char) -> bool` method does exactly that.

Internally the parser maintains a small lookahead buffer that is filled only as needed from the decoder.

## Detecting Stalling

**Trivet** keeps track of the number of times you *peek* at a byte and, if you peek too many times (1,000) without consuming anything, **Trivet** assumes the parse has stalled and panics.  Likewise, if you try to consume too many times (1,000) after the end of file has been reached, **Trivet** will assume the parse is stalled and panics.

## Getting Parse Location

|Method             |Use                            |
|-------------------|-------------------------------|
|`get_loc() -> Loc` |Get current location in parse. |

The current position (the location of the *next* character) is given by `get_loc() -> Loc`.  This returns a structure that contains the name of the source (specified when the parser was created) along with the 1-based line number and 1-based column number.

A useful tactic for parsing large structures is to save the location (the returned `Loc`) with the structure so, if later processing reveals an error, you can cite the source of the structure from the file.  For example, returning to our prior example (in [Building Parsers](building.md)) we could save the location in our tree.

```rust,ignore
#[derive(Debug)]
enum Thing {
    Number(trivet::Loc, u64),
    Id(trivet::Loc, String),
}
```

Then, when we create one of these, we would just include `parser.loc()` as the first element.  At the start of parsing a structure, you may want to save the current location of the parse (`let loc = parser.loc();`) so you can refer to where the structure starts later on.

If you only want the current column, you can get that with `get_column() -> usize`.

## Peeking for Strings

Working byte-by-byte is not very efficient of course, so **Trivet** has other versions of *peek* and *consume*.  The most common ones work based on strings.

|Method                                         |Use    |
|-----------------------------------------------|-------|
|`peek_str(value: &str) -> bool`                |Peek at the next characters of input and return `true` iff they are the string `value` |
|`peek_str_greedy(value: &str) -> bool`         |Peek at the next characters of input and return `true` iff they are the string `value` (but see below) |
|`peek_and_consume_str(value: &str) -> bool`    |Peek and optionally consume (see below) |
|`peek_and_consume_str_ws(value: &str) -> bool` |Peek and optionally consume (see below) |

This is clearly better.  You could look for a keyword with `peek_str("frida")`.  This will return `true` iff the next five characters are `frida`.

What if you want to recognize a string like three double quotation marks: `"""`.  An interesting question is what about *four* double quotation marks `""""`?  Should we match on the first three, or the last three?  Well, `peek_str("\"\"\"")` will do just what you expect; it will match on the first three.  That might not be what you want, so there is also a `peek_str_greedy("\"\"\"")` that will parse this as: `"` and then `"""`.  That is, it will not match on the start of the triple quotation marks, but will match on the end.

More conveniently, `peek_and_consume_str(value: &str) -> bool` peeks at the stream and, if it finds `value` as the next sequence of characters, consumes all characters and then returns `true`.  Otherwise it returns `false`.  The method `peek_and_consume_str_ws(value: &str) -> bool` does the same thing, but also consumes any trailing whitespace.

> Lookahead is limited to the length of the buffer, which is currently 16 KiB.

## Handling Whitespace

As you've seen, **Trivet** requires you to handle whitespace directly.  In reality, parsers always need to know where whitespace is allowed, but **Trivet** just requires you to be a little more explicit about it.

|Method                      |Use    |
|----------------------------|-------|
|`consume_ws_only() -> bool` |Consume all whitespace and leave the parser pointing to the first non-whitespace character. |
|`consume_ws() -> bool`      |Consume all whitespace and leave the parser pointing to the first non-whitespace character; may consume comments |

> Some languages have *significant* whitespace, like [YAML][] and [Python][].  Tracking the initial whitespace can still be done by checking the parse location using `get_column() -> usize` to get the current parse location.  In these cases you may want to handle comments and whitespace separately.

You can consume all whitespace using either of these methods.  This leaves the parser pointing at the first non-whitespace character.  Because `consume_ws() -> bool` also looks for comments, it is a bit slower than `consume_ws_only() -> bool`, but it also handles comments automatically, so it really depends on your application.

It is important to be *consistent* with this.  The common strategy is for a method that parses a specific structure to do the following.

- Assume *on entry* that the parser points to the first character of the thing to parse.
- Consume any trailing whitespace after the structure before returning.
- Add a `_ws` suffix to a method that consumes trailing whitespace.

For more on how comments are handled, see the [section on comments](#handling-comments).

## Handling Comments

**Trivet** comes with support for consuming (and discarding) comments in several different forms.  By default, only C and C++-style comments are parsed.

Comment parsing is actually done by the struct `trivet::comments::CommentParser`.  A comment parser is already installed in an instance of `trivet::Parser`, and you can obtain mutable access to it to configure it using `borrow_comment_parser() -> &mut CommentParser`.

|Method |Use |
|-------|----|
|`borrow_comment_parser() -> &mut CommentParser` |Obtain a mutable reference to the internal comment parser. |

The following forms of comment are supported.

|Comment Style  |Flag                |Default |
|---------------|--------------------|--------|
|`/* ... */`    |`enable_c`          |true    |
|`// ...`       |`enable_cpp`        |true    |
|`<# ... #>`    |`enable_powershell` |false   |
|`# ...`        |`enable_python`     |false   |
|`<!-- ... -->` |`enable_xml`        |false   |

In addition to these comment forms, you can add your own.  Define a method that does the following.

- Accepts a `trivet::ParserCore` instance
- Consumes any comments you wish
- Returns `true` if any comments are consumed

The flag is important; it is used to determine when all comments have been consumed by `CommentParser::process`, which is used by `consume_ws() -> bool`.

Set the `custom` field of the comment parser to a `Box` instance containing your method, and then set
the `enable_custom` field on the comment parser to true.

The following is an example that parses [Lua][] comments, which are a bit notorious for being persnickety.  `--[[` starts a multi-line comment, but `---[[` starts a single-line comment (because the `--` begins a single line comment unless immediately followed by `[[`).

```rust,ignore
use trivet;

let mut parser = trivet::parse_from_string(r#"
    --[[
        I am a long form
        Lua comment.
    --]]"#);
parser.borrow_comment_parser.enable_c = false;
parser.borrow_comment_parser.enable_cpp = false;
parser.borrow_comment_parser.custom = Box::new(
    |parser: &mut trivet::ParserCore| -> bool {
        if parser.peek_and_consume_chars(&vec!['-','-','[','[']) {
            parser.take_until("--]]");
            true
        } else if parser.peek_and_consume_chars(&vec!['-','-']) {
            parser.take_while(|ch| ch != '\n');
            true
        } else {
            false
        }
    }
);
parser.borrow_comment_parser.enable_custom = true;
parser.consume_ws();
assert_eq!(parser.loc().to_string(), "<string>:2:18");
assert!(parser.is_at_eof());
```

Note that inside the body of the closure we use `peek_and_consume_chars(Vec<char>) -> bool` instead of the (simpler) `peek_and_consume_str(&str) -> bool`.  This is because we have to use the `ParserCore` instead of `Parser`, and also because `peek_and_consume_chars(Vec<char>) -> bool` is a bit faster.

## Parsing Sequences

Often you want to parse a stream of characters of a specific class (like digits).  **Trivet** provides convenience methods for this.

|Method  |Use  |
|--------|-------|
|`take_while(Fn(char) -> bool) -> String` |Consume characters while a predicate is true. |
|`take_while_unless(Fn(char) -> bool, Fn(char) -> bool) -> String` |Consume characters, but exclude some. |
|`take_until(Fn(char) -> bool) -> String` |Consume characters until a given predicat is true. |

Suppose you want to parse a hexadecimal number.  You could write the following.

```rust,ignore
let digits = parse.take_while(|ch| ch.is_hexadecimal_digit());
```

The return value will be all hexadecimal digits (if any) that are read.

In Python and some other languages, you can include underscores in numbers to break them into groups, such as `1_235_400` instead of `1235400`.  That is easy to accomplish with a slightly different form.

```rust,ignore
let digits = parse.take_while_unless(|ch| ch.is_digit(), |ch| ch == '_');
```

Here parsing consumes both the digits and the underscores, but only the digits are returned.

Finally, sometimes you want to consume everything until an end token is found, such as the end of line.  You can use `take_until(&str) -> ParseResult<String>` for that.  This method uses the *greedy* version of matching described for `peek_str_greedy(&str) -> ParseResult<bool>` [above](primitives.md#peeking-for-strings), so it will handle things like multi-line Python comments correctly.

```rust,ignore
let text = parse.take_until("\"\"\"")?;
```

[YAML]: https://yaml.org/
[Python]: https://www.python.org/
[Lua]: https://www.lua.org/
