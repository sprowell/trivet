# JSON Example

This chapter will walk through an example of using **Trivet** to build a parser for the JavaScript Object Notation (JSON).  We will build the parser top-down (because that is easier to understand).  In reality, for a more test-driven-development approach, you might wish to build the parser bottom-up so you could test it early and often.

The entire parser is available as an example in the distribution.  See `examples/json.rs`.

## What is JSON?

The JavaScript Object Notation (JSON) is a way of describing data that is very commonly used for data set interchange, and as an input and output format for programs.  A quick description of JSON's structure, along with railroad diagrams of the syntax, can be found [here](https://www.json.org/json-en.html).

JSON is an international standar defined by [ECMA-404: The JSON data interchange syntax](https://www.ecma-international.org/publications-and-standards/standards/ecma-404/).  It is this definition we will use to create our parser.  (Specifically, the Second Edition from December of 2017.)

> The JSON format is pretty strict, and this is good for computer data interchange.  For people writing JSON... it's not as great.  In that case you might take a look at [JSON5](https://json5.org/).

## Representing JSON

A JSON **value** is a single instance of an *object*, *array*, *number*, *string*, *Boolean*, or *null*.  Our parser will read a single value when called.

We will need to represent the JSON content in memory, so let's build an `enum` to do that.

```rust,ignore
#[derive(Debug)]
pub enum JSON {
    Object(HashMap<String,JSON>),
    Array(Vec<JSON>),
    String(String),
    Number(f64),
    Boolean(bool),
    Null,
}
```


We need to be able to write out the JSON structure, and we can create a method to do that, but it is a bit beyond the scope of this chapter since it isn't parsing as such.  We can use `dbg!(value)` to see the content of a JSON value defined by the above.

The example code contained in the `examples` folder of the distribution includes code to print JSON values using the `StringEncoder` struct.

## Main Method and Configuration

Let's create a main method that will start things off and also configure the string and number parsers.

```rust,ignore
use trivet;

pub fn main() {
    let mut parser = trivet::parse_from_stdin();
    parser.parse_comments = false;
    let numpar = parser.borrow_number_parser();
    numpar.permit_binary = false;
    numpar.permit_hexadecimal = false;
    numpar.permit_octal = false;
    numpar.permit_underscores = false;
    let strpar = parser.borrow_string_parser();
    strpar.set(trivet::strings::StringStandard::JSON);
    parser.consume_ws();
    match parse_value_ws(&mut parser) {
        Err(error) => {
            println!("ERROR: {}", error);
        }
        // For fun you can replace the `json_to_string(json)` below with `dbg!(json)`.
        Ok(json) => {
            println!("{}", json_to_string(json));
        }
    }
}
```

First we build a parser around standard input, then we turn off most of the options for the number parser.  JSON does not have comments, so we turn off comment parsing as well.

Finally, we set the string parser to the JSON standard (very convenient in this case).  We parse any leading whitespace, and then parse the next JSON value from the stream and print it.  Now we just need to implement `parse_value_ws(&mut Parser) -> ParseResult<JSON>`.

## Parsing JSON Values

Let's write the method that will parse a single JSON value, given a `Parser` instance.  We will consume any trailing whitespace after the value, so we suffix the method name with `_ws` to indicate this.  Note that we decide what specific thing to parse by looking at the next character in the stream.  Everything is immediately handled here except for *objects* and *arrays*, thanks to the **Trivet** string and number parsers that we configured earlier.

```rust,ignore
/// Parse a JSON value.  On entry the parser is assumed to be pointing at the first
/// character of the value.  Trailing whitespace is consumed.
pub fn parse_value_ws(parser: &mut trivet::Parser) -> trivet::errors::ParseResult<JSON> {
    match parser.peek() {
        // Check for an object.
        '{' => parse_object_ws(parser),

        // Check for an array.
        '[' => parse_array_ws(parser),

        // Check for a string.
        '"' => Ok(JSON::String(parser.parse_string_match_delimiter_ws()?)),

        // Check for a number.
        ch if ch == '-' || ch.is_ascii_digit() => Ok(JSON::Number(parser.parse_f64_ws()?)),

        // Check for Boolean true.
        't' if parser.peek_and_consume_str_ws("true") => Ok(JSON::Boolean(true)),

        // Check for Boolean false.
        'f' if parser.peek_and_consume_str_ws("false") => Ok(JSON::Boolean(false)),

        // Check for null.
        'n' if parser.peek_and_consume_str_ws("null") => Ok(JSON::Null),

        // Error.
        ch => Err(trivet::errors::unexpected_text_error(
            parser.loc(),
            "the start of a JSON value",
            &ch.to_string(),
        )),
    }
}
```

Note that we use the assumption that, on entry, the parser points to the first character of the value to parse.  We then use that character to determine *how* to parse the stream.  This works really well for a language like JSON and, surprisingly, for *most* languages.  We could have written this differently, such as a series of `if-then-else` statements, but the above is simple and obvious, and thus a good choice.

Note how the checks for `true`, `false`, and `null` are implemented.  A check for the first character is very low cost, and that gates the more expensive check for the entire string.  In this case the savings are probably negligible.

Now we need `parse_object_ws(&mut Parser) -> ParseResult<JSON>` and `parse_array_ws(&mut Parser) -> ParseResult<JSON>`.

## Parsing Objects

JSON objects are enclosed in curly braces `{`..`}` and consist of *key*`:`*value* pairs separated by commas `,`.  The key is a double-quoted string, and the value can be any JSON value.

JSON is unforgiving about the format.  Stray or trailing commas, for instance, are not allowed.  This makes parsing the format a bit more complicated, but helps assure that errors are detected early.

The first thing to do is to consume the starting `{`, then consume any whitespace, then start consuming *key*`:`*value* pairs.  We will worry about how to parse strings later.

Our algorithm will look something like this, but with error handling.

```text
consume '{' and trailing whitespace
create a new map
while we have not found '}', do
    if this isn't the first element, parse a ',' and any trailing whitespace
    parse a string, whitespace, a ':', whitespace, and a value
    add the (name,value) pair to the map
done
consume trailing whitespace
return map
```

We use `first` to determine whether to consume a comma or not.

We expect parsing a string to consume trailing white space, and we expect the same for parsing a value, but since this method handles the colons and commas, we must consume whitespace after those.  At the end we consume any whitespace after the closing curly brace.

```rust,ignore
/// Parse a JSON object.  On entry the parser is assumed to be pointing to the first
/// character of the object (a curly brace `{`).  Trailing whitespace is consumed.
fn parse_object_ws(parser: &mut trivet::Parser) -> trivet::errors::ParseResult<JSON> {
    parser.consume();
    parser.consume_ws();
    let mut first = true;
    let mut map = HashMap::new();
    while !parser.peek_and_consume_ws('}') {
        // If not the first element, then we expect to have a comma here.  JSON requires
        // a comma between members of an object.
        if first {
            first = false;
        } else if !parser.peek_and_consume_ws(',') {
            return Err(trivet::errors::syntax_error(
                parser.loc(),
                "There must be a comma (,) between members of an object.",
            ));
        }

        // Look for the quotation mark for a string.
        if parser.peek() != '"' {
            return Err(trivet::errors::syntax_error(
                parser.loc(),
                "Names in a JSON object must be quoted strings.",
            ));
        }
        let name = parser.parse_string_match_delimiter_ws()?;

        // Look for the colon.
        if !parser.peek_and_consume_ws(':') {
            return Err(trivet::errors::syntax_error(
                parser.loc(),
                "Names in a JSON object must be followed by a colon (:).",
            ));
        }

        // Get the value.
        let value = parse_value_ws(parser)?;

        // Add the name, value pair to the map.
        map.insert(name, value);
    }
    Ok(JSON::Object(map))
}
```

On entry we consume the opening brace and any whitespace that follows it.  Then while we haven't hit the closing brace, we parse *key*:*value* pairs.  Each key is a string, and each value is a JSON value, so we use `parse_value_ws(&mut Parser) -> ParseResult<JSON>` recursively to deal with that.

## Parsing Arrays

Now we need to parse arrays.  An array is a comma-separated sequence of JSON values, enclosed in a pair of square brackets `[`..`]`.  We can use almost the same code as we did for objects.  The algorithm is simple.

```text
consume '[' and trailing whitespace
create a new vector
while we have not found ']', do
    if this isn't the first element, parse a ',' and any trailing whitespace
    parse a value
    add the value to the vector
done
consume trailing whitespace
return vector
```

Implementing this and adding error handling gives us the following code.

```rust,ignore
/// Parse a JSON array.  On entry the parser is assumed to be pointing to the first
/// character of the array (a square bracket `[`).  Trailing whitespace is consumed.
fn parse_array_ws(parser: &mut trivet::Parser) -> trivet::errors::ParseResult<JSON> {
    parser.consume();
    parser.consume_ws();
    let mut first = true;
    let mut array = vec![];
    while !parser.peek_and_consume_ws(']') {
        // If not the first element, then we expect to have a comma here.  JSON requires
        // a comma between members of an array.
        if first {
            first = false;
        } else if !parser.peek_and_consume_ws(',') {
            return Err(trivet::errors::syntax_error(
                parser.loc(),
                "There must be a comma (,) between members of an array.",
            ));
        }

        // Get the value.
        let value = parse_value_ws(parser)?;

        // Add the value to the array.
        array.push(value);
    }
    Ok(JSON::Array(array))
}
```

This looks very much like the code we wrote to parse objects.  On entry we consume the square bracket and any following whitespace, then while we haven't found the closing bracket we parse a single JSON value.  These have to be comma separated, so we expect a comma before all but the last value.

We're done.  This is everything we need for the parser.

## Performance

Okay, that's a parser.  How fast is it?

Well, the version you can find in `examples` was written with the goal of making the solution as obvious as possible.  Many things could be done to make it faster.  On my current computer ([Framework Laptop](https://frame.work/) w/12th Gen Intel i7-1260P and 32 GB RAM), it parses about 73 MB of JSON/second.  Is that fast?  Well... it's pretty good.  I should probably run some more samples.

Here's my **Highly Scientific Experiment**.  I have disabled output, so only the parsing and building the JSON structure is timed.

```bash
$ time (cat large.json | target/release/examples/book_json_parser)
real    0m0.358s
user    0m0.281s
sys     0m0.079s
$ ls -l large.json
-rw-rw-r-- 1 sprowell sprowell 26141343 Aug 10 08:08 ../large.json
$ echo $((26141343000/358))
73020511
```

(For fun, if I enable the output of the parsed JSON, the number drops to 40 MB/s.)

A dedicated parser, written to use SIMD instructions and hand-tuned is going to beat it.  *Into*. *The*. *Ground*.  I cite [simdjson](https://github.com/simdjson/simdjson) which runs crazy fast, is written in highly-optimized C++ using SIMD, and achieves an incredible 3 GB/second.

On the other hand, my naïve JSON parser is 113 lines of code (per [Tokei](https://crates.io/crates/tokei)) and I wrote it in a few minutes.
