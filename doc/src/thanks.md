# Thanks

This document was written in [Markdown], with the book itself being generated using [mdBook].  During development [mdbook-linkcheck] was used to verify the links.

[Markdown]: https://daringfireball.net/projects/markdown/syntax
[mdBook]: https://rust-lang.github.io/mdBook/
[mdbook-linkcheck]: https://github.com/Michael-F-Bryan/mdbook-linkcheck
