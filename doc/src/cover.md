<!-- trunk-ignore(markdownlint/MD041) -->
![Trivet](logo.png)

# The Trivet Parsing Library

Stacy Prowell (<mailto:sprowell+trivet@gmail.com>)

Version 2.0

----

## *nominatum*

> A *trivet* is a flat, heat resistant object that serves as a barrier between a hot serving dish and a dining table to protect the table from heat damage.

----

<sub>This book, and the code used to generate it, are Copyright &copy; by Stacy Prowell.  All rights reserved.  This book is distributed under the terms of the Creative Commons [Attribution-ShareAlike 4.0 International][by-sa] license.</sub>

[by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
