fn main() {
    let mut parser = trivet::parse_from_stdin();
    while !parser.is_at_eof() {
        match parser.parse_f64_ws() {
            Err(err) => {
                println!("ERROR: {}", err);
            }
            Ok(value) => {
                println!("  {}", value);
            }
        }
    }
}
