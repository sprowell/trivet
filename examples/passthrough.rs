// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Just a test of the buffering in the parser.

use trivet::errors::ParseResult;
use trivet::parse_from_stdin;

pub fn main() -> ParseResult<()> {
    let mut parser = parse_from_stdin();
    while !parser.is_at_eof() {
        // Read some bytes and then write them out.
        let value = parser.peek_n(57);
        // We can't use value.len() because that is in bytes, not characters.
        for _ in value.chars() {
            parser.consume();
        }
        print!("{}", value);
    }
    Ok(())
}
