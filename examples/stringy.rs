// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! A little interactive string thing.

use std::io::Write;
use trivet::strings::EncodingStandard;
use trivet::strings::StringEncoder;
use trivet::strings::StringParser;
use trivet::strings::StringStandard;

pub fn main() {
    // Make a string parser.
    let mut strpar = StringParser::new();
    let mut encoder = StringEncoder::new();
    println!(
        r#"
Enter strings to process, one per line.  You can switch the string
standard used for parsing by entering any of the following.

  +python +c +json +rust +trivet

Toggle using Unicode names in output with +names.  Toggle encoding
of all non-ascii with +ascii.
    "#
    );

    // Run until we can run no more.  You have to break this with
    // ctrl+c.
    let mut standard = StringStandard::Trivet;
    let mut ascii = false;
    let mut names = false;
    loop {
        let mut line = String::new();
        print!(
            "{}{}{}> ",
            standard,
            if ascii { " (ascii)" } else { "" },
            if names { " (names)" } else { "" },
        );
        let _ = std::io::stdout().flush();
        std::io::stdin().read_line(&mut line).unwrap();
        line.pop();
        match line.as_str() {
            "+python" => {
                standard = StringStandard::Python;
                strpar.set(standard);
            }
            "+rust" => {
                standard = StringStandard::Rust;
                strpar.set(standard);
            }
            "+c" => {
                standard = StringStandard::C;
                strpar.set(standard);
            }
            "+json" => {
                standard = StringStandard::JSON;
                strpar.set(standard);
            }
            "+trivet" => {
                standard = StringStandard::Trivet;
                strpar.set(standard);
            }
            "+names" => {
                names = !names;
            }
            "+ascii" => {
                ascii = !ascii;
            }
            _ => match strpar.parse_string(&line) {
                Ok(value) => {
                    println!("   Debug: {:?}", value);
                    encoder.set(StringStandard::C);
                    if ascii {
                        encoder.encoding_standard = EncodingStandard::ASCII;
                    }
                    if names {
                        encoder.use_names = true;
                    }
                    println!("       C: \"{}\"", encoder.encode(&value));
                    encoder.set(StringStandard::JSON);
                    if ascii {
                        encoder.encoding_standard = EncodingStandard::ASCII;
                    }
                    if names {
                        encoder.use_names = true;
                    }
                    println!("    JSON: \"{}\"", encoder.encode(&value));
                    encoder.set(StringStandard::Rust);
                    if ascii {
                        encoder.encoding_standard = EncodingStandard::ASCII;
                    }
                    if names {
                        encoder.use_names = true;
                    }
                    println!("    Rust: \"{}\"", encoder.encode(&value));
                    encoder.set(StringStandard::Python);
                    if ascii {
                        encoder.encoding_standard = EncodingStandard::ASCII;
                    }
                    if names {
                        encoder.use_names = true;
                    }
                    println!("  Python: \"{}\"", encoder.encode(&value));
                    encoder.set(StringStandard::Trivet);
                    if ascii {
                        encoder.encoding_standard = EncodingStandard::ASCII;
                    }
                    if names {
                        encoder.use_names = true;
                    }
                    println!("  Trivet: \"{}\"", encoder.encode(&value));
                }
                Err(err) => {
                    println!("ERROR: {}", err)
                }
            },
        }
    }
}
