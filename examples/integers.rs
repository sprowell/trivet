// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

use trivet::errors::error;
use trivet::errors::ParseResult;

fn parse_unsigned_integer(parser: &mut trivet::Parser) -> ParseResult<u64> {
    let result = parser.take_while(|ch| ch.is_ascii_digit());
    match result.parse::<u64>() {
        Ok(number) => Ok(number),
        Err(err) => Err(error(parser.loc(), err)),
    }
}

fn main() -> ParseResult<()> {
    // Make a new parser and wrap the standard input.
    let mut parser = trivet::parse_from_stdin();
    match parse_unsigned_integer(&mut parser) {
        Err(err) => {
            println!("{}", err);
        }
        Ok(value) => {
            println!("{}", value);
        }
    }
    Ok(())
}
