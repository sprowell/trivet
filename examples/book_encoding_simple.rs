fn main() {
    let mut encoder = trivet::strings::StringEncoder::new();
    let text_in = "\n\nStringy \u{2020} and \u{1d120}\r\n\u{0}";
    encoder.set(trivet::strings::StringStandard::Trivet);
    println!(" Trivet: {}", encoder.encode(text_in));
    encoder.set(trivet::strings::StringStandard::JSON);
    println!("   JSON: {}", encoder.encode(text_in));
    encoder.set(trivet::strings::StringStandard::Rust);
    println!("   Rust: {}", encoder.encode(text_in));
    encoder.set(trivet::strings::StringStandard::Python);
    println!(" Python: {}", encoder.encode(text_in));
}
