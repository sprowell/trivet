// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Example of parsing an expression.
//!
//! This example is also worked out in The Trivet Parser book.

use trivet::errors::syntax_error;
use trivet::errors::ParseResult;
use trivet::parse_from_stdin;
use trivet::Parser;

/// Parse a number.  The number can be decimal, octal, hexadecimal, or binary.
/// Underscores can be present to break up long groups of digits, and are ignored.
///
/// On entry the parser is assumed to be pointing at the first character.  On
/// exit any trailing whitespace is consumed.
///
pub fn parse_number_ws(parser: &mut Parser) -> ParseResult<i64> {
    // Look for optional radix specifiers.
    let radix = match parser.peek_n(2).as_str() {
        "0x" => {
            parser.consume_n(2);
            16
        }
        "0o" => {
            parser.consume_n(2);
            8
        }
        "0b" => {
            parser.consume_n(2);
            2
        }
        _ => 10,
    };

    // Consume all digits.
    let digits = parser.take_while_unless(|ch| ch.is_ascii_hexdigit(), |ch| ch == '_');

    // Convert the result into a number.
    let value = match i64::from_str_radix(&digits, radix) {
        Ok(value) => value,
        Err(error) => return Err(syntax_error(parser.loc(), &error.to_string())),
    };
    parser.consume_ws();
    Ok(value)
}

/// Parse a primitive expression.  This could be a number or a parenthesized expression.
///
/// On entry the parser is assumed to be pointing at the first character.  On
/// exit any trailing whitespace is consumed.
///
pub fn parse_primitive_ws(parser: &mut Parser) -> ParseResult<i64> {
    // Look for leading minus sign.
    let is_neg = parser.peek_and_consume('-');

    // Look for a parenthesized expression.  Here is where our bottom-up approach
    // otherwise breaks.
    let value = if parser.peek_and_consume('(') {
        let value = parse_top_expr_ws(parser)?;
        if !parser.peek_and_consume_ws(')') {
            return Err(syntax_error(parser.loc(), "Missing closing parenthesis."));
        }
        value
    } else {
        parse_number_ws(parser)?
    };
    Ok(if is_neg { -value } else { value })
}

/// Parse an expression containing exponentiation or bitwise operators.
///
/// On entry the parser is assumed to be pointing at the first character.  On
/// exit any trailing whitespace is consumed.
///
pub fn parse_exp_expr_ws(parser: &mut Parser) -> ParseResult<i64> {
    // Parse an initial primitive.
    let mut left = parse_primitive_ws(parser)?;
    loop {
        // Look for an operator.
        left = if parser.peek_and_consume_ws('^') {
            left ^ parse_primitive_ws(parser)?
        } else if parser.peek_and_consume_ws('|') {
            left | parse_primitive_ws(parser)?
        } else if parser.peek_and_consume_ws('&') {
            left & parse_primitive_ws(parser)?
        } else if parser.peek_and_consume_str_ws("**") {
            let right = parse_primitive_ws(parser)?;
            if right > 0x7fff_ffff {
                // Power is too large.
                return Err(syntax_error(
                    parser.loc(),
                    &format!(
                        "Power {} is too large; must be no larger than {}.",
                        right, 0x7fff_ffff
                    ),
                ));
            }
            if right < 0 {
                // Power is too small.
                return Err(syntax_error(parser.loc(), "Powers must be nonnegative."));
            }
            left.saturating_pow(right as u32)
        } else if parser.peek_and_consume_str_ws(">>") {
            left >> parse_primitive_ws(parser)?
        } else if parser.peek_and_consume_str_ws("<<") {
            left << parse_primitive_ws(parser)?
        } else {
            break;
        }
    }
    Ok(left)
}

/// Parse multiplication, division, and remainder operations.
///
/// On entry the parser is assumed to be pointing at the first character.  On
/// exit any trailing whitespace is consumed.
///
pub fn parse_product_expr_ws(parser: &mut Parser) -> ParseResult<i64> {
    let mut left = parse_exp_expr_ws(parser)?;
    loop {
        left = if parser.peek_and_consume_ws('*') {
            left * parse_exp_expr_ws(parser)?
        } else if parser.peek_and_consume_ws('/') {
            left / parse_exp_expr_ws(parser)?
        } else if parser.peek_and_consume_ws('%') {
            left % parse_exp_expr_ws(parser)?
        } else {
            break;
        }
    }
    Ok(left)
}

/// Parse an expression containing arithmetic and bitwise operators.
///
/// On entry the parser is assumed to be pointing at the first character.  On
/// exit any trailing whitespace is consumed.
///
pub fn parse_top_expr_ws(parser: &mut Parser) -> ParseResult<i64> {
    let mut left = parse_product_expr_ws(parser)?;
    loop {
        left = if parser.peek_and_consume_ws('+') {
            left + parse_product_expr_ws(parser)?
        } else if parser.peek_and_consume_ws('-') {
            left - parse_product_expr_ws(parser)?
        } else {
            break;
        };
    }
    Ok(left)
}

// Main program.
pub fn main() -> ParseResult<()> {
    let mut parser = parse_from_stdin();
    while !parser.is_at_eof() {
        let number = parse_top_expr_ws(&mut parser)?;
        println!("  {}", number);
    }
    Ok(())
}
