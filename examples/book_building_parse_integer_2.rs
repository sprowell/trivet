fn parse_unsigned_integer(parser: &mut trivet::Parser) -> trivet::errors::ParseResult<u64> {
    let result = parser.take_while(|ch| ch.is_ascii_digit());
    match result.parse::<u64>() {
        Ok(number) => Ok(number),
        Err(err) => Err(trivet::errors::error(parser.loc(), err)),
    }
}

fn main() {
    let mut parser = trivet::parse_from_stdin();
    match parse_unsigned_integer(&mut parser) {
        Ok(value) => println!("{}", value),
        Err(err) => println!("ERROR: {}", err),
    }
}
