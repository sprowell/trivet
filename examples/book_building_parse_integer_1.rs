fn parse_unsigned_integer(parser: &mut trivet::Parser) -> trivet::errors::ParseResult<u64> {
    let mut result = String::new();
    while parser.peek().is_ascii_digit() {
        result.push(parser.peek());
        parser.consume();
    }
    match result.parse::<u64>() {
        Ok(number) => Ok(number),
        Err(err) => Err(trivet::errors::error(parser.loc(), err)),
    }
}

fn main() {
    let mut parser = trivet::parse_from_string("16385");
    match parse_unsigned_integer(&mut parser) {
        Ok(value) => println!("{}", value),
        Err(err) => println!("ERROR: {}", err),
    }
}
