fn main() -> trivet::errors::ParseResult<()> {
    let mut parser = trivet::parse_from_stdin();
    while !parser.is_at_eof() {
        // Save the location for later use in error messages.
        let loc = parser.loc();
        // Parse strings only for the supported delimiters.
        let string = match parser.peek() {
            // The initial and final delimiters match.
            '"' | '\'' => parser.parse_string_match_delimiter_ws()?,
            // Here the initial and final delimiters do not match.
            '«' => {
                parser.consume();
                parser.parse_string_until_delimiter_ws('»')?
            }
            // This is not a legal opening delimiter.
            ch => {
                return Err(trivet::errors::syntax_error(
                    loc,
                    &format!("Invalid delimiter: '{}'", ch),
                ));
            }
        };
        println!("  --> {:?}", string);
    }
    Ok(())
}
