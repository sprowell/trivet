use std::io::Read;
use trivet::decoder;

/// This just copies the input to the output.  It can be used to re-encode a file on
/// Windows, but will likely do nothing to a text file on Linux.
pub fn main() {
    // Read and echo the input to the output, decoding the input.
    let mut bytes = vec![];
    std::io::stdin().read_to_end(&mut bytes).unwrap();
    let decode = decoder::Decode::new(bytes);
    for ch in decode {
        print!("{}", ch);
    }
}
