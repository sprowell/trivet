#!/bin/bash

# Trivet
# Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
# https://gitlab.com/binary-tools/trivet

# If you are on *nix-type system like Linux or OS X, then this will run the
# pre-commit pipeline.  If there are *no* warnings or errors, then you are
# (probably) ready to commit.
#
# This is intended to be run from the root of the distribution, so you would
# run it with:
#
# $ . etc/build.sh

cargo install cargo-lichking
cargo install cargo-audit
rustup component add clippy

# Build everything.  This can take a long time, so be prepared.
cargo clean \
    && (yes | python3 etc/heading_updater.py) \
    && cargo fmt \
    && cargo test --features strict \
    && cargo lichking check \
    && cargo audit \
    && cargo clippy --features strict -- -D warnings \
    && RUSTDOCFLAGS="" cargo doc --features strict --no-deps \
    && {
        echo ""
        echo "Ready for pull request."
        echo ""
        echo "Does the version number need to be updated in Cargo.toml???"
        echo "   + bug fix / minor enhancement?  bump PATCH version"
        echo "   + next stop on roadmap?  bump MINOR version"
        echo "   + planned breaking change?  bump MAJOR version"
        echo ""
        echo "If you are modifying the version, you must update the CHANGELOG.md!"
        echo ""
    }
