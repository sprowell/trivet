#!/usr/bin/env bash
#
# Build or update covearge information.
# Use #[cfg_attr(tarpaulin, skip)] to skip lines in source.
#
# This uses tarpaulin to perform coverage testing.  Find tarpaulin here:
# https://crates.io/crates/cargo-tarpaulin
#
# Install tarpaulin with:
# $ cargo install cargo-tarpaulin
#
# The odd echo at the start is there to handle a few doc examples that require
# standard input.  This prevents "hanging" during the coverage testing. It does
# not impact "cargo test".

echo "" | cargo tarpaulin --engine llvm --out xml --run-types tests,doctests \
    && mv cobertura.xml cov.xml
exit $?
