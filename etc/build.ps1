#!/usr/bin/env pwsh

# Trivet
# Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
# https://gitlab.com/binary-tools/trivet

# If you are on a system with PowerShell 7 or greater, then you should consider
# running this script prior to committing for a merge request.  If there are *no*
# warnings or errors, then you are (probably) ready to commit to the development
# branch.
#
# This is intended to be run from the root of the distribution, so you would
# run it with:
#
# $ etc/build.ps1

cargo install cargo-lichking
cargo install cargo-audit
rustup component add clippy

# Try to find Python.  This is way not perfect, but it often works.  The usual
# problem seems to be that some platforms provide "python3" and some provide "python".
# We need 3.5+, so we list it first.
$python = (Get-Command -Name python3,python -ErrorAction 'silentlycontinue')[0].Source
Write-Output "Python is $python"

# Build everything.  This can take a long time, so be prepared.
cargo clean
if (-not $?) { exit 1 }
Invoke-Expression "$python etc/heading_updater.py"
if (-not $?) { exit 1 }
cargo fmt
if (-not $?) { exit 1 }
cargo test --features strict
if (-not $?) { exit 1 }
cargo lichking check
if (-not $?) { exit 1 }
cargo audit
if (-not $?) { exit 1 }
cargo clippy --features strict -- -D warnings
if (-not $?) { exit 1 }
$env:RUSTDOCFLAGS = ''
cargo doc --features strict --no-deps
if (-not $?) { exit 1 }

Write-Output ""
Write-Output "Ready for pull request."
Write-Output ""
Write-Output "Does the version number need to be updated in Cargo.toml???"
Write-Output "   + bug fix / minor enhancement?  bump PATCH version"
Write-Output "   + next stop on roadmap?  bump MINOR version"
Write-Output "   + planned breaking change?  bump MAJOR version"
Write-Output ""
Write-Output "If you are modifying the version, you must update the CHANGELOG.md!"
Write-Output ""
