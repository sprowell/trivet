# Contributing to This Project

Thank you for taking the time to contribute to this project!

The following are the guidelines (and a few actual rules) for contributing to this project.  Use your best judgment and feel free to contact the maintainer directly if you have questions or suggestions.

You can propose changes to *this* document with a merge ("pull") request.  See [How to Contribute](#how-to-contribute) below.

## Code of Conduct

It is important that contributors to a project try to get along.  People working together on a project will disagree, but everyone should be willing to engage in respectful dialog or to disengage until willing.

Please be welcoming, inclusive, and positive toward others.  Unacceptable behavior includes harassment, publishing private information or communication without permission, trolling, and other behavior that could be considered inappropriate.

By contributing to this project you are indicating your acceptance of this code of conduct.

## How to Contribute

Submit corrections, enhancements, bug fixes, and bug reports.  Test code is especially helpful.

The `develop` branch is the default branch.  If you want to contribute, please *create a new branch* off of the `develop` branch for your contribution.  Do not name it `release-*` or `hotfix-*` or `master` or `develop`.  Here's an example of creating a new branch `contribution`.

```bash
$ git checkout -b contribution develop
Switched to a new branch "contribution"
```

Write and thorougly test your contribution.  If this takes a while you will want to pull from `develop` to keep up with other changes and make sure your work is consistent.  Please make sure all code compiles without errors (and preferrably without warnings).  You should use `etc/build.sh` or `etc/build.ps1` to run the pre-commit pipeline.

Once everything is ready, submit a [*merge request*][Merge Requests] to the `develop` branch.  Ideally you will fold your branch into main (so your branch is deleted after the merge).  It may take a while to review your request.  If you read and follow the section [Library Standards](#library-standards) below, it will make this process simpler.

Please include *tests* for your new code.  Tests can be included as documentation tests, or as unit tests included in the module.  Ideally your tests will cover all or most of the lines of code you create or modify.

To prevent fragmentation, all submissions become property of the this project and are subsumed under the existing project copyright and license.  If this is unacceptable please do not contribute, but instead contact the maintainers to explore other arrangements.

## External Libraries

**Trivet** uses no external libraries.  This is a design goal.

## Library Standards

The library has some standards that have been adopted.  It is helpful if contributed code also adopts these standards.  You are welcome to propose better standards!

Be sure to check out the [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/).

### Lines

Source code should have lines no longer than 120 characters.  This is so that it will fit comfortably in a window with a reasonable font size with lots of other stuff open around it.  This does not apply to Markdown files, such as this one, where wrapping lines in an editor is not problematic.

You can display a vertical line in VS Code at column 120 by choosing File->Preferences->Settings, and then adding `"editor.rulers": [120]` to the JSON.

### Comments

Please use complete sentences for all comments.  You might be surprised how much this helps in the long run.

Documentation comments are required for the public API.  Documentation comments are appreciated for the private API, but are not required.

If you write code that allows for a panic, please document that in the method's comments.

Example code that runs as a documentation test is highly desirable.

### Tools

There are two scripts of interest: `etc/build.sh` and `etc/build.ps1`.  The first is a Bash script and the second is a PowerShell script.  Use whichever one you like.

These scripts perform all the pre-commit checks and do some updating to the files (they run `cargo fmt` and update the copyright header in files).  Commit locally or stash any changes *before* you run this script.  Running one of these scripts and getting *no* warnings or errors is the final thing you should do to prepare for submitting a merge request.

These scripts assume you have Python 3.5 or higher installed and available on the path.

> **Aside: Getting Clippy**
> Code should compile with *no* warnings and certainly *no* errors.  Format it with `cargo fmt`, and make sure no warnings or errors are reported by `cargo clippy`.  You may need to install `clippy`.  At present that is done with:
>
> ```bash
> $ rustup component add clippy
> ```

It is *recommended* that you run [Lizard][] to check the overall code complexity and do not submit if you get warnings.

> **Aside: Getting Lizard**
> Lizard is written in Python and can be installed with `pip install lizard`.  Run it with `lizard -lrust src`.  If you don't want to see the code count, etc., then use the `-w` switch.

It is *recommended* that you run [cargo-careful][] to check for additional issues.  Run it with `cargo +nightly careful test`.

> **Aside: Getting cargo-careful**
> This is a dynamic checker for Rust projects.  You will need to install `cargo-careful`.  At present that is done with:
>
> ```bash
> $ cargo install cargo-careful
> ```
>
> You will also need to be sure rust *nightly* is installed.

### Error Suppression Methods

Some code *necessarily* generates a `Result` type with a possible error because it uses routines that *might* generate an error.  If the error is not consequential, a `_noerr` variant should be provided that explicitly suppresses the error.  For example, code that just prints a banner might look as follows.

```rust
pub fn banner(buffer: &mut dyn Write) -> Result<(), Error> {
    write!(buffer, /* text... */)
}

pub fn banner_noerr(buffer: &mut dyn Write) {
    let _ = banner(buffer);
}
```

Often the `_noerr` variant can be used.

### Using TODO

The Todo Tree extension is used to manage a list of "to do" and "fix me" entries in the code.  Just add a comment that starts with `TODO`.  Note that `BUG` is also supported but, in general, it would be better to just fix the bug instead of marking it.  Of course, sometimes bugs are really hard to fix, so tagging suspect locations might be worthwhile.  The tags seem to be ignored in `//!` style comments, as they are not *documentation* but *[technical debt]*.

Larger-scope changes go in either [Roadmap](Roadmap.md) or in [TechDebt](TechDebt.md).  The former is for stuff related to a specific planned version *or* to be placed on a "wish list," while the latter is more important and must be completed prior to the next release.  Don't put the same thing on both lists: stuff on the roadmap should not be on the technical debt list, and vice-versa.

## Release Process

The following is the release checklist.

  1. [ ] Make sure there is no remaining [technical debt](TechDebt.md).
  2. [ ] Verify that the [roadmap](Roadmap.md) is up to date and milestones are met.
  3. [ ] Make sure the [changelog](CHANGELOG.md) is up to date.
  4. [ ] Correct the version of Rust in the [GitLab CI](.gitlab-ci.yml) configuration.
  5. [ ] Make sure everything is checked into the `dev` branch.
  6. [ ] Run `etc/build.sh` or `etc/build.ps1` and verify no errors or warnings.
  7. [ ] Update the version number found in the [cargo build](Cargo.toml).
  8. [ ] Check in to `dev`.
  9. [ ] Tag the repository with the release number.
  10. [ ] Issue a merge request to the `master` branch.

[Lizard]: https://github.com/terryyin/lizard
[Merge Requests]: https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html
[technical debt]: https://en.wikipedia.org/wiki/Technical_debt
[cargo-careful]: https://github.com/RalfJung/cargo-careful
