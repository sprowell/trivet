# Trivet

> A *trivet* is a flat, heat resistant object that serves as a barrier between a hot serving dish and a dining table to protect the table from heat damage.

**Trivet** is a Rust library of tools for writing [recursive descent parsers][].  These are *low level tools*.  **Trivet** is *not* an easy to use library for writing a parser for a complex language, and is not a good choice for languages that need unbounded lookahead... but then what is?

## Why Trivet

**Trivet** is *probably* not the parsing library you want.  You don't write a nice description of your language and then generate a parser from that description.  No.  **Trivet** gives you tools to peek at the stream and consume characters from it... and that's pretty much it.

**Trivet** has *no dependencies* other than the Rust standard libraries, and the entire code base is tiny.  It even deals with the odd encoding on Windows.

If all that sounds good, then Welcome!

**Trivet** uses [Semantic Versioning][].

## Bugs?

Have you found a bug?  Submit it through the [Trivet Issues][] tracker, and be sure to include (1) what happens, (2) what you *expect* to happen, (3) how to *reproduce* the bug, and (4) what *version* of **Trivet** you are using.

## Online Documentation

For the master branch only:

* [The Trivet Book](https://binary-tools.gitlab.io/trivet/book/html/)
* [Trivet Rust API](https://binary-tools.gitlab.io/trivet/trivet/)

## Local Documentation

**If you have built the documentation locally**, then this link will take you to it.

* [Local Trivet Book](book.html)
* [Local Trivet Rust API](apidocs.html)

## Building

You need to have Rust 2021 edition installed to compile.  Compilation should be relatively simple.

```bash
$ cargo build
   Compiling trivet v2.0.0 (/home/Projects/trivet)
    Finished dev [unoptimized + debuginfo] target(s) in 0.14s
```

This should build the **Trivet** library.

You might have a look at `etc/build.sh` and `etc\build.ps1`.  These are little scripts (in bash and powershell) that run the "pre-commit" pipeline.

## Trivet Book

Trivet has a book describing its use.  To build the book you need [mdBook][] and [mdbook-linkcheck][].  You can install these as follows.

```bash
$ cargo install mdbook mdbook-linkcheck
    Updating crates.io index
  Downloaded mdbook v0.4.34
  Downloaded 1 crate (1.5 MB) in 0.95s
  ...
     Summary Successfully installed mdbook-linkcheck, mdbook!
```

Once these are installed you can build the book with the following command.

```bash
$ mdbook build doc
2023-08-08 07:11:00 [INFO] (mdbook::book): Book building has started
2023-08-08 07:11:00 [INFO] (mdbook::book): Running the html backend
2023-08-08 07:11:00 [INFO] (mdbook::book): Running the linkcheck backend
2023-08-08 07:11:00 [INFO] (mdbook::renderer): Invoking the "linkcheck" renderer
```

You can also automatically rebuild the book on any changes with the following.

```bash
$ mdbook serve doc
2023-08-08 07:11:23 [INFO] (mdbook::book): Book building has started
2023-08-08 07:11:23 [INFO] (mdbook::book): Running the html backend
...
2023-08-08 07:11:23 [INFO] (warp::server): listening on http://127.0.0.1:3000
```

## API Documentation

If you want documentation run `cargo doc --no-deps`.

After generation the API documentation should be available locally starting at [`target/doc/trivet/index.html`](target/doc/trivet/index.html).

## Security

Please submit sensitive security issues using the "This issue is confidential..." button below the issue description field in the issue tracker.

The code should be checked, periodically, against the known Rust security advisories.  This can be done with `cargo audit`.  You can install that with `cargo install cargo-audit`.

Consider installing [Trivy][].  This is a vulnerability scanner.

## Contribute

See [Contributing][] for information about contributing to this project.

If you are interested in contributing, see the [Roadmap][], the [TechDebt][], and any `TODO` or `BUG` comments in the code for some things that still need to be done.  Others may be working on them, so be sure to check in with the project.

## Copyright

**Trivet** is Copyright (c) 2023 by Stacy Prowell <mailto:sprowell+trivet@gmail.com>.  All rights reserved.  See the [Copyright][] and [License][] files for more information.

[Copyright]:            ./COPYRIGHT
[License]:              ./LICENSE
[TechDebt]:             ./TechDebt.md
[Roadmap]:              ./Roadmap.md
[Contributing]:         ./CONTRIBUTING.md
[Trivet Issues]:        https://gitlab.com/binary-tools/trivet/-/issues
[Semantic Versioning]:  https://semver.org/
[Trivy]:                https://aquasecurity.github.io/trivy/
[recursive descent parsers]: https://en.wikipedia.org/wiki/Recursive_descent_parser
[mdBook]: https://rust-lang.github.io/mdBook/
[mdbook-linkcheck]: https://github.com/Michael-F-Bryan/mdbook-linkcheck
