// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! **Trivet** is a library for building parsers.  It is not a
//! parser generator; it is a parser *facilitator*.  It provides
//! a collection of low-level routines with which you can build
//! recursive descent parsers if you are careful.
//!
//! This is the API documentation for **Trivet**.  For a better
//! guide to building parsers with this library, see the
//! [The Trivet Parsing Library](https://binary-tools.gitlab.io/trivet/book/html/).
//!
//! Comments, bugs, feature requrests, and instructions on
//! contributing to this project can be found on the web at
//! [the project repository](https://binary-tools.gitlab.io/trivet/).
//!
//! The primary item of interest here is the [`Parser`] struct,
//! which provides the parsing primitives with which you can build
//! recursive descent parsers.
//!
//! There are special methods to construct parsers around different sources.
//!
//!   * [`parse_from_string()`] creates a parser for a `&str` source
//!   * [`parse_from_bytes()`] creates a parser for a `&[u8]` source
//!   * [`parse_from_path()`] creates a parser for a `PathBuf` source
//!   * [`parse_from_stdin()`] creates a parser for the standard input
//!
//! Quick access to parsing or encoding a string and to parsing numbers is
//! available via the [`Tools`] struct.
//!
//! # Library Functionality
//!
//! Features of this parser library include line and column tracking, processing
//! of both UTF-8 and UTF-16 input, including partial parsing of corrupt files (a
//! Unicode failure does not break the entire parse).
//!
//! # Comments, Strings, and Numbers
//!
//! Comments can be parsed automatically by the [`Parser`], and multiple kinds
//! of comments are directly supported.  See the documentation for [`Parser`]
//! for details.
//!
//! Strings can be parsed by a configurable [`strings::StringParser`].  This
//! supports many different string encoding standards.  This approach provides
//! a lot of flexibility, but if you have a specific string encoding in mind,
//! a dedicated parser for that standard will likely be faster.  The
//! [`strings::StringEncoder`] provides for writing strings in different ways.
//!
//! Numbers can be parsed by a configurable [`numbers::NumberParser`].  Again,
//! this provides a lot of different encoding standards.
//!
//! # Crate Features
//!
//! Specific settings can be enabled or disabled using features in `Cargo.toml`.
//! These features (except `legacy`) should be considered *experimental* and you
//! should not depend on them.
//!
//! |Feature |Meaning |
//! |--------|--------|
//! |`legacy` |Enable support for the old (deprecated) API (pre 2.0) |
//! |`no_stall_detection` |Disable stall detection for parsers |
//! |`no_tracking` |Disable line and column tracking |
//! |`no_ucd` |Do not build in the Unicode name database |
//! |`uppercase_hex` |Prefer uppercase hexadecimal numbers |
//! |`strict` |Unused at present |
//!
//! Features are discussed in [The Rust Book](https://doc.rust-lang.org/cargo/reference/features.html).

#![doc(html_logo_url = "https://gitlab.com/binary-tools/trivet/-/raw/develop/etc/trivet.png")]

pub mod decoder;
pub mod errors;

// These two modules are private, but the items in them are imported and made
// available at the top level.
mod core;
mod loc;
mod tools;

// Make tools available.
pub use crate::tools::Tools;

// Additional public modules that provide specialized parsing.
pub mod comments;
pub mod numbers;
pub mod strings;

pub use crate::core::*;
pub use crate::loc::Loc;

// Include the legacy API for the parser.
#[cfg(feature = "legacy")]
pub mod parser;

// Testing.
#[cfg(test)]
mod tests;
