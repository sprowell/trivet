// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Convert numbers to a hex representation.  This is intended to be faster than the
//! string-based conversions.
//!
//! All results are returned in the order for output in increasing index.  That is, the
//! first character to write is in index 0, the next in index 1, and so forth.

#[cfg(not(feature = "uppercase_hex"))]
const OFF: u8 = 0x27;
#[cfg(feature = "uppercase_hex")]
const OFF: u8 = 0x7;

/// Convert the given byte into a pair of characters.  The high-order character is at
/// index 0.
pub fn byte_to_two_digit_hex(value: u8) -> [char; 2] {
    let mut low = (value & 0xf) | 0x30;
    if low > 0x39 {
        low += OFF
    };
    let mut high = (value >> 4) | 0x30;
    if high > 0x39 {
        high += OFF
    };
    [high as char, low as char]
}

/// Convert the given character to a pair of surrogate code points.  If the given character is not
/// in the correct range (above `0xffff`) then the results will be nonsense!  The highest nybble is
/// at index 0.
pub fn char_to_surrogate_pair(value: char) -> [char; 8] {
    let number = value as u32;
    let pair = (((number - 0x10000) & 0xffc00) << 6) | (number & 0x3ff) | 0xd800_dc00;
    let b1 = {
        let bt = (pair & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b2 = {
        let bt = (pair >> 4 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b3 = {
        // This will always be at least C.
        ((pair >> 8 & 0xf) as u8 | 0x30) + OFF
    };
    let b4 = {
        // This will always be at least D.
        let bt = (pair >> 12 & 0xf) as u8 | 0x30;
        bt + OFF
    };
    let b5 = {
        let bt = (pair >> 16 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b6 = {
        let bt = (pair >> 20 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b7 = {
        // This will always be no greater than 9.
        (pair >> 24 & 0xf) as u8 | 0x30
    };
    let b8 = {
        // This will always be at least D.
        ((pair >> 28 & 0xf) as u8 | 0x30) + OFF
    };
    [
        b8 as char, b7 as char, b6 as char, b5 as char, b4 as char, b3 as char, b2 as char,
        b1 as char,
    ]
}

/// Convert the given character to four hexadecimal digits.  If the given character is not in the correct
/// range (below `0x10000`) then the result will be nonsense!  The high-order character is at index 0.
pub fn char_to_four_digit_hex(value: char) -> [char; 4] {
    let number = value as u32;
    let b1 = {
        let bt = (number & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b2 = {
        let bt = (number >> 4 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b3 = {
        let bt = (number >> 8 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b4 = {
        let bt = (number >> 12 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    [b4 as char, b3 as char, b2 as char, b1 as char]
}

/// Convert the given character to six hexadecimal digits.  This should completely cover the current
/// Unicode range.  The high-order character is at index 0.
pub fn char_to_six_digit_hex(value: char) -> [char; 6] {
    let number = value as u32;
    let b1 = {
        let bt = (number & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b2 = {
        let bt = (number >> 4 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b3 = {
        let bt = (number >> 8 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b4 = {
        let bt = (number >> 12 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b5 = {
        let bt = (number >> 16 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b6 = {
        // This last position will never be very large with the current Unicode system, so there
        // is no need to check for conversion to a letter.
        (number >> 20 & 0xf) as u8 | 0x30
    };
    [
        b6 as char, b5 as char, b4 as char, b3 as char, b2 as char, b1 as char,
    ]
}

/// Convert a u32 value (such as a char) into an eight hexadecimal character array.  The high-order
/// character is at index 0.  Thus `hex_dword(0x1044_3120)` becomes
/// `['1','0','4','4','3','1','2','0']`.  This routine contains no loops or memory lookups.
pub fn dword_to_eight_digit_hex(value: u32) -> [char; 8] {
    let b1 = {
        let bt = (value & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b2 = {
        let bt = (value >> 4 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b3 = {
        let bt = (value >> 8 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b4 = {
        let bt = (value >> 12 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b5 = {
        let bt = (value >> 16 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b6 = {
        let bt = (value >> 20 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b7 = {
        let bt = (value >> 24 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    let b8 = {
        let bt = (value >> 28 & 0xf) as u8 | 0x30;
        if bt > 0x39 {
            bt + OFF
        } else {
            bt
        }
    };
    [
        b8 as char, b7 as char, b6 as char, b5 as char, b4 as char, b3 as char, b2 as char,
        b1 as char,
    ]
}

#[cfg(test)]
mod test {
    use crate::strings::hex::char_to_six_digit_hex;

    use super::byte_to_two_digit_hex;
    use super::char_to_four_digit_hex;
    use super::char_to_surrogate_pair;
    use super::dword_to_eight_digit_hex;

    #[test]
    fn hex_byte_test() {
        assert_eq!(byte_to_two_digit_hex(0x00), ['0', '0']);
        assert_eq!(byte_to_two_digit_hex(0x01), ['0', '1']);
        assert_eq!(byte_to_two_digit_hex(0x10), ['1', '0']);
        #[cfg(not(feature = "uppercase_hex"))]
        {
            assert_eq!(byte_to_two_digit_hex(0xee), ['e', 'e']);
            assert_eq!(byte_to_two_digit_hex(0xff), ['f', 'f']);
            assert_eq!(byte_to_two_digit_hex(0xf0), ['f', '0']);
            assert_eq!(byte_to_two_digit_hex(0x0f), ['0', 'f']);
        }
        #[cfg(feature = "uppercase_hex")]
        {
            assert_eq!(byte_to_two_digit_hex(0xee), ['E', 'E']);
            assert_eq!(byte_to_two_digit_hex(0xff), ['F', 'F']);
            assert_eq!(byte_to_two_digit_hex(0xf0), ['F', '0']);
            assert_eq!(byte_to_two_digit_hex(0x0f), ['0', 'F']);
        }
        assert_eq!(byte_to_two_digit_hex(0x55), ['5', '5']);
        assert_eq!(byte_to_two_digit_hex(0x45), ['4', '5']);
        assert_eq!(byte_to_two_digit_hex(0x54), ['5', '4']);
        assert_eq!(byte_to_two_digit_hex(0x80), ['8', '0']);
    }

    #[test]
    fn char_to_four_digit_hex_test() {
        assert_eq!(char_to_four_digit_hex('\u{0000}'), ['0', '0', '0', '0']);
        assert_eq!(char_to_four_digit_hex('\u{000f}'), ['0', '0', '0', 'f']);
        assert_eq!(char_to_four_digit_hex('\u{00f0}'), ['0', '0', 'f', '0']);
        assert_eq!(char_to_four_digit_hex('\u{0f00}'), ['0', 'f', '0', '0']);
        assert_eq!(char_to_four_digit_hex('\u{f000}'), ['f', '0', '0', '0']);
    }

    #[test]
    fn char_to_six_digit_hex_test() {
        assert_eq!(
            char_to_six_digit_hex('\u{000000}'),
            ['0', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            char_to_six_digit_hex('\u{00000f}'),
            ['0', '0', '0', '0', '0', 'f']
        );
        assert_eq!(
            char_to_six_digit_hex('\u{0000f0}'),
            ['0', '0', '0', '0', 'f', '0']
        );
        assert_eq!(
            char_to_six_digit_hex('\u{000f00}'),
            ['0', '0', '0', 'f', '0', '0']
        );
        assert_eq!(
            char_to_six_digit_hex('\u{00f000}'),
            ['0', '0', 'f', '0', '0', '0']
        );
        assert_eq!(
            char_to_six_digit_hex('\u{0f0000}'),
            ['0', 'f', '0', '0', '0', '0']
        );
        assert_eq!(
            char_to_six_digit_hex('\u{10ffff}'),
            ['1', '0', 'f', 'f', 'f', 'f']
        );
    }

    #[test]
    fn hex_dword_test() {
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_0000),
            ['0', '0', '0', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_0001),
            ['0', '0', '0', '0', '0', '0', '0', '1']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_0020),
            ['0', '0', '0', '0', '0', '0', '2', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_0300),
            ['0', '0', '0', '0', '0', '3', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_4000),
            ['0', '0', '0', '0', '4', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0005_0000),
            ['0', '0', '0', '5', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0060_0000),
            ['0', '0', '6', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0700_0000),
            ['0', '7', '0', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x8000_0000),
            ['8', '0', '0', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_0000),
            ['0', '0', '0', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_000f),
            ['0', '0', '0', '0', '0', '0', '0', 'f']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_00e0),
            ['0', '0', '0', '0', '0', '0', 'e', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_0d00),
            ['0', '0', '0', '0', '0', 'd', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0000_c000),
            ['0', '0', '0', '0', 'c', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x000b_0000),
            ['0', '0', '0', 'b', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x00a0_0000),
            ['0', '0', 'a', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0x0b00_0000),
            ['0', 'b', '0', '0', '0', '0', '0', '0']
        );
        assert_eq!(
            dword_to_eight_digit_hex(0xc000_0000),
            ['c', '0', '0', '0', '0', '0', '0', '0']
        );
    }

    #[test]
    fn char_to_surrogate_test() {
        assert_eq!(
            char_to_surrogate_pair('\u{10037}'),
            ['d', '8', '0', '0', 'd', 'c', '3', '7']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{3ffff}'),
            ['d', '8', 'b', 'f', 'd', 'f', 'f', 'f']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{20234}'),
            ['d', '8', '4', '0', 'd', 'e', '3', '4']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{189e2}'),
            ['d', '8', '2', '2', 'd', 'd', 'e', '2']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{32001}'),
            ['d', '8', '8', '8', 'd', 'c', '0', '1']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{1000f}'),
            ['d', '8', '0', '0', 'd', 'c', '0', 'f']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{100f0}'),
            ['d', '8', '0', '0', 'd', 'c', 'f', '0']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{10f00}'),
            ['d', '8', '0', '3', 'd', 'f', '0', '0']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{1f000}'),
            ['d', '8', '3', 'c', 'd', 'c', '0', '0']
        );
        assert_eq!(
            char_to_surrogate_pair('\u{30000}'),
            ['d', '8', '8', '0', 'd', 'c', '0', '0']
        );
    }
}
