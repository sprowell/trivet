// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Module for string encoding and decoding.

mod decoder;
mod encoder;
mod escape_standards;
mod hex;
mod ucd;

pub use decoder::*;
pub use encoder::*;
pub use ucd::*;

pub use escape_standards::EncodingMethod;
pub use escape_standards::EncodingStandard;
pub use escape_standards::EscapeType;
pub use escape_standards::IllegalUnicodeProtocol;
pub use escape_standards::StringStandard;
pub use escape_standards::UnknownEscapeProtocol;

use escape_standards::C_ESCAPES;
use escape_standards::JSON_ESCAPES;
use escape_standards::PYTHON_ESCAPES;
use escape_standards::RUST_ESCAPES;
use escape_standards::TRIVET_ESCAPES;
