// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Provide easy access to some simple tools from the library.

use crate::{
    decoder::Decode,
    errors::ParseResult,
    numbers::NumberParser,
    strings::{StringEncoder, StringParser, StringStandard},
    ParserCore,
};

/// Provide quick access to some simple utilities without having to create a parser, etc.
///
/// To use this, make an instance.  If you want a particular string standard, use
/// [`Self::set_string_standard`] to select it.  If you need deeper configuration, then
/// directly access the fields.
pub struct Tools {
    /// The number decoder to use.
    pub number_parser: NumberParser,
    /// The string parser to use.
    pub string_parser: StringParser,
    /// The string encoder to use.
    pub string_encoder: StringEncoder,
}

impl Tools {
    /// Make a new instance.
    pub fn new() -> Self {
        Tools {
            number_parser: NumberParser::new(),
            string_parser: StringParser::new(),
            string_encoder: StringEncoder::new(),
        }
    }

    /// Set the string standard.
    pub fn set_string_standard(&mut self, std: StringStandard) -> &mut Self {
        self.string_parser.set(std);
        self
    }

    /// Parse the argument as a string, processing escapes, etc.  The entire content of the
    /// string is parsed.  For details on this, see the [`crate::strings::StringParser`]
    /// struct.  Think of this as converting a `\n` into a newline.
    ///
    /// ```rust
    /// # fn main() -> trivet::errors::ParseResult<()> {
    /// let result = trivet::Tools::new().parse_string(r#"\u{2020}\r\n"#)?;
    /// assert_eq!(result, "†\r\n");
    /// # Ok(())
    /// # }
    /// ```
    pub fn parse_string(&self, body: &str) -> ParseResult<String> {
        self.string_parser.parse_string(body)
    }

    /// Encode the argument as a string, expanding characters into escape codes as needed.
    /// The entire content of the string is encoded.  See [`crate::strings::StringEncoder`]
    /// for details.  Think of this as turning a newline into `\n`.
    ///
    /// ```rust
    /// # fn main() {
    /// let result = trivet::Tools::new().encode_string("\u{2020}\r\n");
    /// assert_eq!(result, r#"†\r\n"#);
    /// # }
    /// ```
    pub fn encode_string(&self, body: &str) -> String {
        self.string_encoder.encode(body)
    }

    /// Parse the string as an i128.  If a radix indicator is present at the start, then
    /// the string is parsed in that radix.
    ///
    /// ```rust
    /// # fn main() -> trivet::errors::ParseResult<()> {
    /// let value = trivet::Tools::new().parse_i128("-0xe021_18f0")?;
    /// assert_eq!(value, -0xe021_18f0);
    /// # Ok(())
    /// # }
    /// ```
    pub fn parse_i128(&self, body: &str) -> ParseResult<i128> {
        let decoder = Decode::from_string(body);
        let mut parser = ParserCore::new("", decoder);
        self.number_parser.parse_i128(&mut parser)
    }

    /// Parse the string as a u128.  If a radix indicator is present at the start, then
    /// the string is parsed in that radix.
    ///
    /// ```rust
    /// # fn main() -> trivet::errors::ParseResult<()> {
    /// let value = trivet::Tools::new().parse_u128("0xe021_18f0")?;
    /// assert_eq!(value, 0xe021_18f0);
    /// # Ok(())
    /// # }
    /// ```
    pub fn parse_u128(&self, body: &str) -> ParseResult<u128> {
        let decoder = Decode::from_string(body);
        let mut parser = ParserCore::new("", decoder);
        self.number_parser.parse_u128(&mut parser)
    }

    /// Parse the string as a f64.  If a radix indicator is present at the start, then
    /// the string is parsed in that radix.
    ///
    /// ```rust
    /// # fn main() -> trivet::errors::ParseResult<()> {
    /// let value = trivet::Tools::new().parse_f64("0.65e-14")?;
    /// assert_eq!(value, 0.65e-14);
    /// # Ok(())
    /// # }
    /// ```
    pub fn parse_f64(&self, body: &str) -> ParseResult<f64> {
        let decoder = Decode::from_string(body);
        let mut parser = ParserCore::new("", decoder);
        self.number_parser.parse_f64(&mut parser)
    }
}

impl Default for Tools {
    fn default() -> Self {
        Self::new()
    }
}
