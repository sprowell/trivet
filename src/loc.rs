// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Provide a struct to package location information during a parse.

use std::fmt;

/// Capture a location in a parse.
///
/// Typically parsers create instances of `Loc` and error messages
/// consume them.  However, it is also possible to look up where a
/// particular item is declared, and that may be helpful.
///
/// If you are not parsing a file, the simplest way to use this is to
/// just make an "internal" instance.
///
/// ```rust
/// use trivet::Loc;
///
/// let location = Loc::Internal;
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Loc {
    /// This indicates that the source of an item is internal to the
    /// library; that is, it does not arise from parsing.
    Internal,

    /// This indicates that the source of an item is an unnamed stream.
    /// This might be appropriate for reading from standard input, for
    /// instance.
    Console {
        /// Line number, starting at one.
        line: usize,

        /// Column number, starting at one.
        column: usize,
    },

    /// This indicates that the source of an item is a named stream,
    /// such as a file or URL.
    File {
        /// Stream name.
        name: String,

        /// Line number, starting at one.
        line: usize,

        /// Column number, starting at one.
        column: usize,
    },
}

impl fmt::Display for Loc {
    fn fmt(&self, form: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Internal => write!(form, "(internal)"),
            Self::Console { line, column } => write!(form, "{}:{}", line, column),
            Self::File { name, line, column } => write!(form, "{}:{}:{}", name, line, column),
        }
    }
}
