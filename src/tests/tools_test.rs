// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Test the tools struct.

use crate::{errors::ParseResult, Tools};

#[test]
fn tools_test() -> ParseResult<()> {
    let mut tools = Tools::default();
    tools.set_string_standard(crate::strings::StringStandard::Rust);
    assert_eq!(tools.parse_f64("-19.01e2")?, -19.01e2);
    assert_eq!(tools.parse_i128("7_635")?, 7635);
    assert_eq!(tools.parse_u128("0b1011101010")?, 0b1011101010);
    Ok(())
}
