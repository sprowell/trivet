// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Tests of the loc module.

use crate::loc::Loc;

#[test]
fn internal() {
    let loc1 = Loc::Internal;
    let loc2 = Loc::Internal;
    let loc3 = Loc::Console {
        line: 17,
        column: 21,
    };
    let loc4 = Loc::Console {
        line: 17,
        column: 21,
    };
    let loc5 = Loc::Console {
        line: 81,
        column: 21,
    };
    let loc6 = Loc::Console {
        line: 17,
        column: 17,
    };
    let loc7 = Loc::File {
        name: "".to_string(),
        line: 17,
        column: 21,
    };
    let loc8 = Loc::File {
        name: "".to_string(),
        line: 17,
        column: 21,
    };
    let loc9 = Loc::File {
        name: "".to_string(),
        line: 81,
        column: 21,
    };
    let loc10 = Loc::File {
        name: "".to_string(),
        line: 17,
        column: 17,
    };
    let loc11 = Loc::File {
        name: "tom".to_string(),
        line: 17,
        column: 21,
    };
    let loc12 = Loc::File {
        name: "tom".to_string(),
        line: 17,
        column: 21,
    };
    let loc13 = Loc::File {
        name: "tom".to_string(),
        line: 81,
        column: 21,
    };
    let loc14 = Loc::File {
        name: "tom".to_string(),
        line: 17,
        column: 17,
    };

    assert_eq!(loc1.to_string(), "(internal)");
    assert_eq!(loc2.to_string(), "(internal)");
    assert_eq!(loc3.to_string(), "17:21");
    assert_eq!(loc4.to_string(), "17:21");
    assert_eq!(loc5.to_string(), "81:21");
    assert_eq!(loc6.to_string(), "17:17");
    assert_eq!(loc7.to_string(), ":17:21");
    assert_eq!(loc8.to_string(), ":17:21");
    assert_eq!(loc9.to_string(), ":81:21");
    assert_eq!(loc10.to_string(), ":17:17");
    assert_eq!(loc11.to_string(), "tom:17:21");
    assert_eq!(loc12.to_string(), "tom:17:21");
    assert_eq!(loc13.to_string(), "tom:81:21");
    assert_eq!(loc14.to_string(), "tom:17:17");

    assert_eq!(loc1, loc1);
    assert_eq!(loc1, loc2);
    assert_ne!(loc1, loc3);

    assert_eq!(loc3, loc3);
    assert_eq!(loc3, loc4);
    assert_ne!(loc3, loc5);
    assert_ne!(loc3, loc6);
    assert_ne!(loc3, loc7);

    assert_eq!(loc7, loc7);
    assert_eq!(loc7, loc8);
    assert_ne!(loc7, loc9);
    assert_ne!(loc7, loc10);
    assert_ne!(loc7, loc11);

    assert_eq!(loc11, loc11);
    assert_eq!(loc11, loc12);
    assert_ne!(loc11, loc13);
    assert_ne!(loc11, loc14);
}
