// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Tests of the parser module.

use crate::decoder::Decode;
use crate::errors::ParseResult;
use crate::parse_from_bytes;
use crate::parse_from_path;
use crate::parse_from_string;
use crate::Parser;
use std::env::temp_dir;
use std::fs::remove_file;
use std::fs::File;
use std::io::Write;

#[test]
fn console() {
    // Check that the correct loc is returned for console.  File locs are
    // tested several times elsewhere.
    let decoder = Decode::new(vec![]);
    let parser = Parser::new("", decoder);
    assert_eq!(parser.loc().to_string(), "1:1");
}

#[test]
fn trivial() {
    // Test correct handling of an empty stream at start.
    let decoder = Decode::new(vec![]);
    let parser = Parser::new("<string>", decoder);
    assert!(parser.is_at_eof());
}

#[test]
fn eof() {
    let mut parser = parse_from_string("G");
    assert_eq!(parser.loc().to_string(), "<string>:1:1");
    assert_eq!(parser.peek(), 'G');
    parser.consume();
    assert_eq!(parser.loc().to_string(), "<string>:1:2");
    // We're at the end of file, but don't know that until we
    // force the parser to look.
    assert!(parser.is_at_eof());
}

#[test]
fn take_while() {
    let mut parser = parse_from_string("Can we find the end?");
    assert_eq!(parser.take_until("C"), "");
    assert_eq!(parser.peek(), 'a');
    assert_eq!(
        parser.take_while(|ch| ch.is_alphanumeric() || ch == ' '),
        "an we find the end"
    );
}

#[test]
fn take_while_unless() {
    let mut parser = parse_from_string("Can we find the end?");
    assert_eq!(parser.take_until("C"), "");
    assert_eq!(parser.peek(), 'a');
    assert_eq!(
        parser.take_while_unless(|ch| ch.is_alphanumeric(), |ch| ch.is_whitespace()),
        "anwefindtheend"
    );
}

#[test]
fn check() {
    let mut parser = parse_from_string("Can we find the end?");
    assert!(!parser.peek_chars(&"Ban".chars().collect()));
    assert!(parser.peek_chars(&"Can".chars().collect()));
    assert!(parser.peek_str("Can"));
    assert!(!parser.peek_str("Flan"));
    assert!(parser.peek_chars(&"Can we".chars().collect()));
}

#[test]
fn peek_n() {
    let mut parser = parse_from_string("Can we find the end?");
    assert_eq!(parser.peek_n(4), "Can ");
    parser.consume_n(4);
    assert_eq!(parser.peek_n(4), "we f");
    parser.consume_n(4);
    assert_eq!(parser.peek_n(4), "ind ");
    parser.consume_n(4);
    assert_eq!(parser.peek_n(4), "the ");
    parser.consume_n(4);
    assert_eq!(parser.peek_n(4), "end?");
    parser.consume_n(4);
    assert!(parser.is_at_eof());
}

#[test]
#[should_panic]
fn catch_stall_peek() {
    let mut parser = parse_from_string("Can we find the end?");
    while parser.peek() == 'C' {
        // Do nothing.
    }
}

#[test]
#[should_panic]
fn catch_stall_peek_2() {
    let mut parser = parse_from_string("Can we find the end?");
    loop {
        parser.peek_n(17);
    }
}

#[test]
#[should_panic]
fn catch_stall_eof() {
    let mut parser = parse_from_string("C");
    loop {
        parser.consume();
    }
}

#[test]
#[should_panic]
fn catch_stall_eof_2() {
    let mut parser = parse_from_string("C");
    loop {
        parser.consume_n(31);
    }
}

#[test]
fn peek_and_consume() {
    let mut parser =
        parse_from_string("the rain \n /*in*/ spain falls\t\t\tmainly/* */ on the plain");
    assert_eq!(parser.loc().to_string(), "<string>:1:1");
    parser.consume();
    assert_eq!(parser.loc().to_string(), "<string>:1:2");
    assert_eq!(parser.peek(), 'h');
    parser.consume_n(6);
    assert_eq!(parser.peek(), 'n');
    assert_eq!(parser.take_until("*/"), "n \n /*in");
    assert_eq!(parser.peek(), ' ');
    parser.consume();
    assert_eq!(parser.peek(), 's');
    assert_eq!(parser.peek_n(5), "spain");
    assert!(!parser.peek_str("spaim"));
    assert!(!parser.peek_and_consume_str("spain  "));
    assert!(parser.peek_and_consume_str("spain"));
    assert_eq!(parser.loc().to_string(), "<string>:2:14");
    assert!(!parser.peek_and_consume_str(" falz"));
    assert!(parser.peek_and_consume_str(" falls"));
    assert!(parser.consume_ws_only());
    assert!(!parser.consume_ws_only());
    assert!(!parser.consume_ws_only());
    assert!(!parser.peek_and_consume('n'));
    assert!(parser.peek_and_consume_str("mainly"));
    assert_eq!(parser.take_until("*/"), "/* ");
    assert!(parser.consume_ws_only());
    assert!(parser.peek_str("on the plain"));
}

#[test]
fn take() {
    let mut parser = parse_from_bytes(b"any given day:104_218,ab7ac-8ab9--4");
    assert_eq!(
        parser.take_while(|ch| ch.is_alphabetic() || ch.is_whitespace()),
        "any given day"
    );
    assert!(parser.peek_and_consume(':'));
    assert_eq!(
        parser.take_while_unless(|ch| ch.is_numeric(), |ch| ch == '_'),
        "104218"
    );
    assert!(parser.peek_and_consume(','));
    assert_eq!(parser.take_until("--"), "ab7ac-8ab9");
}

#[test]
fn consume() {
    let mut parser = parse_from_bytes(b"any given day:104_218,ab7ac-8ab9--4");
    assert!(parser.consume_while(|ch| ch.is_alphabetic() || ch.is_whitespace()));
    assert!(parser.peek_and_consume(':'));
    assert!(!parser.consume_while(|ch| ch.is_alphabetic()));
    assert!(parser.consume_while(|ch| { ch.is_numeric() || ch == '_' }));
    assert!(parser.peek_and_consume(','));
    assert!(parser.consume_until("--"));
    assert!(parser.peek_str("4"));
}

#[test]
fn file() {
    // Get a file path.
    let mut buf = temp_dir();
    buf.push("trivet-test.tmp");

    // Remove the file if it exists.
    let _ = remove_file(buf.clone());

    // Create a file.
    let mut file = File::create(buf.clone()).expect("Unable to create file");
    write!(
        file,
        r#"
        {{
            "Ḽơᶉëᶆ" : "ȋṕšᶙṁ" ,
            "ḍỡḽǭᵳ" : [ "ʂǐť", "ӓṁệẗ" ] ,
            "Lorem" : {{
                "ipsum":["dolor","sit","amet"]
            }}
        }}
        "#
    )
    .expect("Unable to write to file");
    let _ = file.flush();

    // Parse the file.
    let mut parser = parse_from_path(&buf).expect("Unable to open file.");
    let thing = parser.take_while_unless(|_| true, |ch| ch.is_whitespace());
    assert_eq!(
        thing,
        r#"{"Ḽơᶉëᶆ":"ȋṕšᶙṁ","ḍỡḽǭᵳ":["ʂǐť","ӓṁệẗ"],"Lorem":{"ipsum":["dolor","sit","amet"]}}"#
    );
}

#[test]
fn peek_edge() {
    // Edge case for peek_n.
    let mut parser = parse_from_bytes(b"any ");
    assert_eq!(parser.peek_n(0), "");
    assert!(parser.peek_str(""));
    assert!(parser.peek_chars(&Vec::<char>::new()));
    assert_eq!(parser.peek_n(10947), "any ");
    assert!(!parser.peek_chars(&vec!['a', 'n', 'y', ' ', '\0']));
}

#[test]
fn takes() {
    // Edge case for peek_n.
    let mut parser = parse_from_bytes(b"xyzzy \t\n\r 92_8346_46trivet");
    assert_eq!(parser.take_while(|ch| ch.is_alphabetic()), "xyzzy");
    assert_eq!(parser.take_while(|ch| ch.is_whitespace()), " \t\n\r ");
    assert_eq!(
        parser.take_while_unless(|ch| ch.is_ascii_digit(), |ch| ch == '_'),
        "92834646"
    );
    assert_eq!(parser.take_until("<"), "trivet");
    assert!(parser.is_at_eof());
}

#[test]
fn consume_2() {
    // Edge case for peek_n.
    let mut parser = parse_from_bytes(b"xyzzy \t\n\r 92_8346_46trivet");
    assert!(parser.peek_and_consume_chars(&vec!['x', 'y', 'z', 'z', 'y', ' ']));
    assert!(!parser.peek_and_consume_ws('#'));
    assert!(parser.peek_and_consume_ws('\t'));
    let mut parser = parse_from_bytes(b"xyzzy \t\n\r 92_8346_46trivet");
    assert!(!parser.peek_and_consume_str_ws("xyzzo"));
    assert!(parser.peek_and_consume_str_ws("xyzzy"));
    assert_eq!(parser.peek(), '9');
    assert!(parser.consume_until("ve"));
    assert_eq!(parser.peek(), 't');
}

#[test]
fn comments() {
    // Test a little comment processing.
    let text = r#"
    // This is a comment.
    # This is also a comment, and should be skipped.
    /* This is a longer comment.
       Second line of longer comment. */
    token
    # Final comment.
    "#;
    let mut parser = parse_from_string(text);
    assert!(parser.consume_ws());
    assert!(!parser.consume_ws());
    assert_eq!(parser.peek(), '#');
    parser.borrow_comment_parser().enable_python = true;
    assert!(parser.consume_ws());
    assert!(!parser.consume_ws());
    assert!(parser.peek_and_consume_chars_ws(&vec!['t', 'o', 'k', 'e', 'n']));
    assert!(parser.is_at_eof());
    let mut parser = parse_from_string(text);
    parser.borrow_comment_parser().enable_python = false;
    parser.borrow_comment_parser().enable_c = false;
    parser.borrow_comment_parser().enable_cpp = false;
    assert!(parser.consume_ws());
    assert_eq!(parser.peek(), '/');
}

#[test]
fn greedy() {
    // Test greedy peek.
    let mut parser = parse_from_string("''''''''");
    while !parser.is_at_eof() && !parser.peek_str_greedy("'''") {
        parser.consume();
    }
    // Consume the end marker.
    parser.consume_n(3);
    assert!(parser.is_at_eof());
}

#[test]
fn embedded_parsers() -> ParseResult<()> {
    let mut parser = parse_from_string("\"This is a string\"0x17.4");
    let numpar = parser.borrow_number_parser();
    numpar.permit_binary = false;
    numpar.permit_octal = false;
    let strpar = parser.borrow_string_parser();
    strpar.set(crate::strings::StringStandard::Trivet);
    parser.consume();
    let string = parser.parse_string_until_delimiter('"')?;
    assert_eq!(string, "This is a string");
    let number = parser.parse_f64()?;
    assert_eq!(number, 23.25);
    let mut parser = parse_from_string("17_21  0x21  -12  0b101 48.5 1.1e-7");
    assert_eq!(parser.parse_u128()?, 1721);
    parser.consume_ws_only();
    assert_eq!(parser.parse_u128_ws()?, 0x21);
    assert_eq!(parser.parse_i128()?, -12);
    parser.consume_ws_only();
    assert_eq!(parser.parse_i128_ws()?, 5);
    assert_eq!(parser.parse_f64()?, 48.5);
    parser.consume_ws_only();
    assert_eq!(parser.parse_f64_ws()?, 1.1e-7);
    Ok(())
}

#[test]
fn parse_items() -> ParseResult<()> {
    let mut parser = parse_from_string(r#""Down in Ohio" 2112 -2112 -21.12"#);
    parser.parse_comments = false;
    assert_eq!(parser.parse_string_match_delimiter_ws()?, "Down in Ohio");
    assert_eq!(parser.parse_u128_ws()?, 2112);
    assert_eq!(parser.parse_i128_ws()?, -2112);
    assert_eq!(parser.parse_f64_ws()?, -21.12);

    let mut parser = parse_from_string(r#""#);
    assert_eq!(parser.parse_string_match_delimiter()?, "");
    assert_eq!(parser.parse_string_match_delimiter_ws()?, "");

    let mut parser = parse_from_string(r#" This is unusual « 776"#);
    assert_eq!(
        parser.parse_string_until_delimiter_ws('«')?,
        " This is unusual "
    );
    assert_eq!(parser.parse_u128()?, 776);
    assert_eq!(parser.parse_string(r#"\r\n\t\t"#)?, "\r\n\t\t");
    Ok(())
}
