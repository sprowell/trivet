// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Test number encoding and decoding.

use crate::decoder::Decode;
use crate::errors::ParseResult;
use crate::numbers::{NumberParser, Radix};
use crate::ParserCore;

fn test_str(string: &str) -> ParserCore {
    let decoder = Decode::from_string(string);
    ParserCore::new("<string>", decoder)
}

#[test]
fn signed_ok() -> ParseResult<()> {
    let mut numpar = NumberParser::new();
    numpar.permit_underscores = true;
    let text_in = "  -12_986  65_536  -16444 -1 0 0_0";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, -12986);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, 65536);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, -16444);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, -1);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, 0);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, 0);
    let text_in = "-170_141_183_460_469_231_731_687_303_715_884_105_728";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert_eq!(
        numpar.parse_i128(&mut parser)?,
        -170_141_183_460_469_231_731_687_303_715_884_105_728
    );
    let text_in = "170_141_183_460_469_231_731_687_303_715_884_105_727";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert_eq!(
        numpar.parse_i128(&mut parser)?,
        170_141_183_460_469_231_731_687_303_715_884_105_727
    );
    numpar.permit_underscores = false;
    let text_in = "170141183460469231731687303715884105727";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert_eq!(
        numpar.parse_i128(&mut parser)?,
        170_141_183_460_469_231_731_687_303_715_884_105_727
    );
    Ok(())
}

#[test]
fn unsigned_ok() -> ParseResult<()> {
    let mut numpar = NumberParser::new();
    numpar.permit_underscores = true;
    let text_in = "  12_986  65_536  16444 1 0 0_0";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 12986);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 65536);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 16444);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 1);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 0);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 0);
    let text_in = "340_282_366_920_938_463_463_374_607_431_768_211_456";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_u128(&mut parser).is_err());
    numpar.permit_underscores = false;
    let text_in = "340282366920938463463374607431768211456";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_u128(&mut parser).is_err());
    Ok(())
}

#[test]
fn signed_err() -> ParseResult<()> {
    let mut numpar = NumberParser::new();
    numpar.permit_underscores = true;
    let text_in = "";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_i128(&mut parser).is_err());
    let text_in = "A";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_i128(&mut parser).is_err());
    let text_in = "999_999_999_999_999_999_999_999_999_999_999_999_999";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_i128(&mut parser).is_err());
    let text_in = "170_141_183_460_469_231_731_687_303_715_884_105_728";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_i128(&mut parser).is_err());
    Ok(())
}

#[test]
fn unsigned_err() -> ParseResult<()> {
    let mut numpar = NumberParser::new();
    numpar.permit_underscores = true;
    let text_in = "";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_u128(&mut parser).is_err());
    let text_in = "A";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_u128(&mut parser).is_err());
    let text_in = "999_999_999_999_999_999_999_999_999_999_999_999_999";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_u128(&mut parser).is_err());
    let text_in = "340_282_366_920_938_463_463_374_607_431_768_211_456";
    let mut parser = test_str(text_in);
    let _ = parser.consume_ws_only();
    assert!(numpar.parse_u128(&mut parser).is_err());
    Ok(())
}

#[test]
fn radix_strings() {
    assert_eq!(Radix::Binary.to_string(), "Binary");
    assert_eq!(Radix::Octal.to_string(), "Octal");
    assert_eq!(Radix::Decimal.to_string(), "Decimal");
    assert_eq!(Radix::Hexadecimal.to_string(), "Hexadecimal");
}

#[test]
fn radices() -> ParseResult<()> {
    let numpar = NumberParser::new();
    let text_in = "0xff 0xff_ff -0xfff_f 0b10110 -0x000_1 0o21 -0o21";
    let mut parser = test_str(text_in);
    assert_eq!(numpar.parse_i128(&mut parser)?, 0xff);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, 0xffff);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, -0xffff);
    parser.consume_ws_only();
    assert_eq!(parser.peek_n(7), "0b10110");
    assert_eq!(numpar.parse_i128(&mut parser)?, 0b10110);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, -0x1);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, 0o21);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_i128(&mut parser)?, -0o21);
    parser.consume_ws_only();
    let text_in = "0xff 0xff_ff 0xfff_f 0b10110 0x000_1 0o210";
    let mut parser = test_str(text_in);
    assert_eq!(numpar.parse_u128(&mut parser)?, 0xff);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 0xffff);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 0xffff);
    parser.consume_ws_only();
    assert_eq!(parser.peek_n(7), "0b10110");
    assert_eq!(numpar.parse_u128(&mut parser)?, 0b10110);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 0x1);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_u128(&mut parser)?, 0o210);
    parser.consume_ws_only();
    let text_in = "ff ff_ff -fff_f 10110 -000_1 21 -21";
    let mut parser = test_str(text_in);
    assert_eq!(
        numpar.parse_u128_radix(&mut parser, &Radix::Hexadecimal)?,
        0xff
    );
    let _ = parser.consume_ws_only();
    assert_eq!(
        numpar.parse_u128_radix(&mut parser, &Radix::Hexadecimal)?,
        0xffff
    );
    let _ = parser.consume_ws_only();
    assert_eq!(
        numpar.parse_i128_radix(&mut parser, &Radix::Hexadecimal)?,
        -0xffff
    );
    let _ = parser.consume_ws_only();
    assert_eq!(
        numpar.parse_i128_radix(&mut parser, &Radix::Binary)?,
        0b10110
    );
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128_radix(&mut parser, &Radix::Binary)?, -0b1);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_u128_radix(&mut parser, &Radix::Octal)?, 0o21);
    let _ = parser.consume_ws_only();
    assert_eq!(numpar.parse_i128_radix(&mut parser, &Radix::Octal)?, -0o21);
    Ok(())
}

#[test]
fn floats_1() -> ParseResult<()> {
    let numpar = NumberParser::new();
    let text_in = "12 12e2 -12 -12e2 12e-2 -12e-2 12p-2 -12p-2 12e+2";
    let mut parser = test_str(text_in);
    assert_eq!(numpar.parse_f64(&mut parser)?, 12.0);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, 1200.0);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, -12.0);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, -1200.0);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.12);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, -0.12);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.12);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, -0.12);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, 1200.0);
    parser.consume_ws_only();
    let text_in = "0b0.1 0o0.4 0.5 0x.8";
    let mut parser = test_str(text_in);
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.5);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.5);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.5);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.5);
    parser.consume_ws_only();
    Ok(())
}

#[test]
fn floats_2() -> ParseResult<()> {
    let numpar = NumberParser::new();
    let text_in = "0x0.400_000_000_000_000_000_000_000_000_000_000";
    let mut parser = test_str(text_in);
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.25);
    parser.consume_ws_only();
    let text_in = "0x00006.5p-3";
    let mut parser = test_str(text_in);
    assert_eq!(numpar.parse_f64(&mut parser)?, 0.001_541_137_695_312_5);
    parser.consume_ws_only();
    let text_in = "0x6.5p-9_999_999_999";
    let mut parser = test_str(text_in);
    assert!(numpar.parse_f64(&mut parser).is_err());
    parser.consume_ws_only();
    let text_in = "0x.p2";
    let mut parser = test_str(text_in);
    assert!(numpar.parse_f64(&mut parser).is_err());
    parser.consume_ws_only();
    Ok(())
}

#[test]
fn floats_3() -> ParseResult<()> {
    let numpar = NumberParser::new();
    let text_in = "inf -inf infinity nan";
    let mut parser = test_str(text_in);
    assert_eq!(numpar.parse_f64(&mut parser)?, f64::INFINITY);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, -f64::INFINITY);
    parser.consume_ws_only();
    assert_eq!(numpar.parse_f64(&mut parser)?, f64::INFINITY);
    parser.consume_ws_only();
    assert!(numpar.parse_f64(&mut parser)?.is_nan());
    parser.consume_ws_only();
    Ok(())
}
