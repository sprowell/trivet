// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Provide the legacy (pre 2.0) parsing API.
//!
//! To use this, enable the `legacy` feature in `Cargo.toml` or include
//! `--features legacy` on the command line for `cargo`.
//!
//! **Warning**: This is a *legacy* API included to support software developed with
//! the 1.1 version of **Trivet**.  ***Do not use this for future development!***
//! See [`crate::Parser`] for the API post-2.0.

use crate::comments::CommentParser;
use crate::decoder::Decode;
pub use crate::errors::ParseError;
pub use crate::errors::ParseResult;
use crate::parse_from_bytes as pfb;
use crate::parse_from_path as pfp;
use crate::parse_from_stdin as pft;
use crate::parse_from_string as pfs;
pub use crate::Loc;
use crate::Parser as NewParser;
use std::fs::File;
use std::io::Read;
use std::io::Stdin;
use std::marker::PhantomData;
use std::path::PathBuf;

/// Provide methods to implement a recursive descent parser.
///
/// This struct wraps a stream (an instance of `Read`) and
/// provides two primitives.
///
///   * You can "peek" at upcoming characters in the stream.
///   * You can "consume" characters from the stream.
///
/// These primitives are used to construct a series of more
/// complex methods, but essentially all you can do is look
/// at what is coming up in the stream, and then consume and
/// discard characters from the stream.
///
/// # Whitespace
///
/// Aside from methods specifically to consume whitespace, there
/// are special versions of some "consume" methods that consume whitespace
/// following a match.  These methods end with `_ws` and consume
/// any *trailing* whitespace following a successful match.
///
/// Whitespace includes only those characters that satisfy the
/// `char::is_whitespace()` predicate.  By default, *comments*
/// count as whitespace, so consuming whitespace will also consume
/// comments.  This behavior can be disabled by setting the
/// `comments_are_whitespace` flag to false.
///
/// # Comments
///
/// The parser supports many different comment forms.  You set these
/// by specifying the `single_line_comments` and `multi_line_comment`
/// fields.
///
/// **Note**: There are two important caveats.  First, the parser does not process *nested*
/// comments, such as those used in Standard ML.  Second, comments where there is ambiguity
/// are not correctly handled, so the Lua forms are problematic.  You will need to write your
/// own processor for those cases.
///
/// # Example
///
/// To use this, make an instance with [`Self::new()`], giving a stream and
/// a name for the stream.  The name given will be used when
/// constructing [`Loc`] instances.  If the name is the empty string,
/// then `Console` locations are created; otherwise `File` locations
/// are created.
///
/// Use [`Self::peek()`] to peek at the next character in the stream, and
/// use [`Self::consume()`] to discard that character.  Note that [`Self::peek()`]
/// returns a result, and may be `Ok(None)` if at the end of stream.
/// The following uses these two methods to ignore whitespace
/// (though there actually is a method just for that).
///
/// ```rust
/// use std::io;
/// use trivet::parser::{Parser, ParseResult};
///
/// # fn main() -> ParseResult<()> {
/// // Make a new parser around the standard input.
/// let mut parser = Parser::new("", io::stdin());
///
/// // While the next character is whitespace, consume it.
/// while match parser.peek()? {
///     None => false,
///     Some(ch) => ch.is_whitespace(),
/// } {}
///
/// // The stream should now be pointing to the first
/// // non-whitespace character.
/// # Ok(())
/// # }
/// ```
///
/// This would be a very tedious way to parse anything, so there
/// are other methods to help.  In particular, [`Self::consume_whitespace()`]
/// will do the job just outlined above.
///
/// More interestingly you can look to see if the next thing in the
/// stream is a given string with [`Self::peek_str()`] and [`Self::peek_str_greedy()`].
/// Usually once you match an upcoming string, you probably want to
/// consume it.  To both check for the string and consume it if
/// you find it, use [`Self::peek_and_consume()`] (for single characters) or
/// [`Self::peek_and_consume_str()`] (for strings).
///
/// You may also want to consume a sequence of characters, such as
/// all the digits or letters in a stream.  The method [`Self::take_while()`]
/// will do that.  It takes a predicate on characters (usually a
/// lambda) and consumes characters while the predicate is true.  All
/// the characters are then returned.  The following uses this to
/// obtain the next decimal integer from the stream.
///
/// ```rust
/// use std::io;
/// use trivet::parser::{Parser, ParseResult};
///
/// # fn main() -> ParseResult<()> {
/// // Make a new parser around the standard input.
/// let mut parser = Parser::new("", io::stdin());
///
/// // Look for a minus sign.
/// let minus = parser.peek_and_consume('-')?;
///
/// // Read the next integer from the stream.
/// let digits = parser.take_while(|ch| ch.is_digit(10))?;
/// let number: i64 = (if minus { -1 } else { 1 })
///     * digits.parse().unwrap_or(0);
///
/// // The stream now points to the first non-digit character.
/// # Ok(())
/// # }
/// ```
#[deprecated]
pub struct Parser<R: Read> {
    parser: NewParser,
    comments_are_whitespace: bool,
    single_line_comments: Vec<String>,
    multi_line_comments: Vec<(String, String)>,
    phantom: PhantomData<R>,
}

/// Create a parser from a byte slice.  The source name is set
/// to `<bytes>`.
#[deprecated]
#[allow(deprecated)]
pub fn parse_from_bytes(source: &[u8]) -> Parser<&[u8]> {
    Parser::wrap(pfb(source))
}

/// Create a parser from a string.  The source name is set to
/// `<string>`.
#[deprecated]
#[allow(deprecated)]
pub fn parse_from_string(source: &str) -> Parser<&[u8]> {
    Parser::wrap(pfs(source))
}

/// Create a parser for the standard input.  The source is set
/// to the console.
#[deprecated]
#[allow(deprecated)]
pub fn parse_from_stdin() -> Parser<Stdin> {
    Parser::wrap(pft())
}

/// Create a parser for the given file.  The source name is
/// set to the given path.
#[deprecated]
#[allow(deprecated)]
pub fn parse_from_path(path: &PathBuf) -> ParseResult<Parser<File>> {
    let name = path.to_str().unwrap().to_string();
    match pfp(path) {
        Result::Ok(parser) => ParseResult::Ok(Parser::wrap(parser)),
        Result::Err(error) => ParseResult::Err(ParseError::Error {
            loc: Loc::File {
                name,
                line: 1,
                column: 1,
            },
            cause: Box::new(error),
        }),
    }
}

#[allow(deprecated)]
impl<R: Read> Parser<R> {
    /// Create a new parser.
    ///
    /// This sets defaults, including treating comments as whitespace and
    /// processing C (`/*`..`*/`) and C++ (`//`) comments only.
    pub fn new(name: &str, source: R) -> Self {
        let mut bytes = vec![];
        let mut src = source;
        let _ = src.read_to_end(&mut bytes);
        let decoder = Decode::new(bytes);
        Self::wrap(NewParser::new(name, decoder))
    }

    /// Create a new old-API parser around a new parser.
    pub fn wrap(parser: NewParser) -> Self {
        // We are going to ignore the comment fields and rely on the default
        // parser to handle things for us.
        let compar = CommentParser::new();
        let mut newp = parser;
        newp.replace_comment_parser(compar);
        Parser {
            parser: newp,
            phantom: PhantomData,
            comments_are_whitespace: true,
            single_line_comments: vec!["//".to_string()],
            multi_line_comments: vec![("/*".to_string(), "*/".to_string())],
        }
    }

    /// Determine if the parser has reached the end of the stream.
    #[inline(always)]
    pub fn is_at_eof(&self) -> bool {
        self.parser.is_at_eof()
    }

    /// Get an unexpected end of file parse error.
    pub fn unexpected_eof_error(&self) -> ParseError {
        let ioe = std::io::Error::new(std::io::ErrorKind::UnexpectedEof, "Unexpected end of file");
        ParseError::Io {
            loc: self.loc(),
            cause: ioe,
        }
    }

    /// Get a parse error with the specified message.
    pub fn syntax_error(&self, msg: &str) -> ParseError {
        ParseError::Syntax {
            msg: msg.to_string(),
            loc: self.loc(),
        }
    }

    /// Get a parse error with information about expected things.
    pub fn unexpected_text_error(&self, expected: &str, found: &str) -> ParseError {
        ParseError::Unexpected {
            loc: self.loc(),
            expected: expected.to_owned(),
            found: found.to_owned(),
        }
    }

    /// Get a parse error with information about an unexpected character.
    pub fn unexpected_character_error(&self, expected: &str, found: char) -> ParseError {
        ParseError::Unexpected {
            loc: self.loc(),
            expected: expected.to_owned(),
            found: format!("'{}'", found),
        }
    }

    /// Get an internal error.
    pub fn internal_error(&self, msg: &str) -> ParseError {
        ParseError::Internal {
            msg: msg.to_owned(),
            loc: self.loc(),
        }
    }

    /// Something mentioned in the parse could not be found.
    pub fn not_found_error(&self, what: &str) -> ParseError {
        ParseError::NotFound {
            loc: self.loc(),
            what: what.to_owned(),
        }
    }

    /// An I/O error occurred.
    pub fn io_error(&self, cause: std::io::Error) -> ParseError {
        ParseError::Io {
            loc: self.loc(),
            cause,
        }
    }

    /// Something was referenced but it is not yet implemented.
    pub fn unimplemented_error(&self, thing: &str) -> ParseError {
        ParseError::Unimplemented {
            loc: self.loc(),
            thing: thing.to_owned(),
        }
    }

    /// Wrap another error as a parse error.
    pub fn error<E>(&self, err: E) -> ParseError
    where
        E: std::error::Error + 'static,
    {
        ParseError::Error {
            loc: self.loc(),
            cause: Box::new(err),
        }
    }

    /// Get the current location of the parse.  This can return two
    /// different kinds of locations.  If the name was empty when the
    /// parser was created, it returns a [`Loc::Console`] instance.
    /// If the name was not empty, it returns a [`Loc::File`] instance.
    pub fn loc(&self) -> Loc {
        self.parser.loc()
    }

    /// Consume a single character.
    #[inline(always)]
    pub fn consume(&mut self) {
        self.parser.consume()
    }

    /// Consume characters.
    pub fn consume_n(&mut self, n: usize) {
        self.parser.consume_n(n)
    }

    /// Peek at the next character available.
    ///
    /// If the end of file is reached, `None` is returned.  Otherwise the next available
    /// character is returned, but nothing is consumed.
    pub fn peek(&mut self) -> ParseResult<Option<char>> {
        if self.parser.is_at_eof() {
            Ok(None)
        } else {
            Ok(Some(self.parser.peek()))
        }
    }

    /// Peek at the next few characters in the unwind buffer and return them.
    ///
    /// The result is returned as a string.  Nothing is consumed by this metehod.
    pub fn peek_n(&mut self, n: usize) -> ParseResult<String> {
        Ok(self.parser.peek_n(n))
    }

    /// Peek ahead and see if the provided string is present in the stream.
    #[inline(always)]
    pub fn peek_str(&mut self, value: &str) -> ParseResult<bool> {
        Ok(self.parser.peek_str(value))
    }

    /// Peek ahead and see if the provided string is present in the stream.
    /// This method returns false if the token is also at an offset of one,
    /// which may seem an odd choice, but allows matching end tokens where
    /// there are repeated characters, such as `"""`.
    #[inline(always)]
    pub fn peek_str_greedy(&mut self, value: &str) -> ParseResult<bool> {
        Ok(self.parser.peek_str_greedy(value))
    }

    /// Look ahead and if the next character matches the provided value, consume
    /// it and return true.  Otherwise do not consume anything and return false.
    pub fn peek_and_consume(&mut self, ch: char) -> ParseResult<bool> {
        Ok(self.parser.peek_and_consume(ch))
    }

    /// Look ahead and if the next character matches the provided value, consume
    /// it and return true.  Otherwise do not consume anything and return false.
    /// If the specified character is found, then any trailing whitespace is also
    /// consumed.
    pub fn peek_and_consume_ws(&mut self, ch: char) -> ParseResult<bool> {
        Ok(if self.parser.peek_and_consume_ws(ch) {
            let _ = self.consume_whitespace();
            true
        } else {
            false
        })
    }

    /// Look ahead and if the next characters match the provided value, consume
    /// them and return true.  Otherwise do not covalue: Stringnsume anything and return false.
    pub fn peek_and_consume_str(&mut self, value: &str) -> ParseResult<bool> {
        Ok(self.parser.peek_and_consume_str(value))
    }

    /// Look ahead and if the next characters match the provided value, consume
    /// them and return true.  Otherwise do not consume anything and return false.
    /// If the specified string is found, any trailing whitespace is also consumed.
    pub fn peek_and_consume_str_ws(&mut self, value: &str) -> ParseResult<bool> {
        Ok(if self.parser.peek_and_consume_str_ws(value) {
            let _ = self.consume_whitespace();
            true
        } else {
            false
        })
    }

    /// Consume all whitespace and comments.
    ///
    /// **This method is deprecated, and will be removed.**  Instead, use either
    /// `consume_comments()`, which implements the same functionality but is shorter
    /// to type, or `consume_whitespace()` with the `comments_are_whitespace` field
    /// set to `true`.
    pub fn consume_whitespace_and_comments(&mut self) -> ParseResult<()> {
        let _ = self.consume_comments();
        Ok(())
    }

    /// Consume all whitespace from the current position, leaving the first
    /// non-whitespace character (if any) as the next character to read.
    pub fn consume_whitespace(&mut self) -> ParseResult<bool> {
        if self.comments_are_whitespace {
            self.consume_comments()
        } else {
            Ok(self.parser.consume_ws_only())
        }
    }

    /// Consume and return characters while a given test returns true.
    ///
    /// For example, to consume all digits you might use the following.
    ///
    /// ```rust
    /// # use trivet::parser::parse_from_string;
    /// # use trivet::parser::Parser;
    /// # use trivet::parser::ParseResult;
    /// # fn main() -> ParseResult<()> {
    /// # #[allow(deprecated)]
    /// let mut parser = parse_from_string("6748673fred");
    /// let digits = parser.take_while(|ch| ch.is_ascii_digit())?;
    /// assert_eq!(digits, "6748673");
    /// # Ok(())
    /// # }
    /// ```
    pub fn take_while<T: Fn(char) -> bool>(&mut self, test: T) -> ParseResult<String> {
        Ok(self.parser.take_while(test))
    }

    /// Consume and return characters while a given test returns true, skipping any characters for which
    /// the exclude test is true.  This continues until *both* tests fail.
    ///
    /// The exclude test is checked first, so the following will match letters but exclude vowels from
    /// the result returned.
    ///
    /// ```rust
    /// # use trivet::parser::parse_from_string;
    /// # use trivet::parser::Parser;
    /// # use trivet::parser::ParseResult;
    /// # fn main() -> ParseResult<()> {
    /// let mut parser = parse_from_string("The quick brown fox jumps over the lazy dog");
    /// let value = parser.take_while_unless(|ch| ch.is_alphabetic() || ch == ' ', |ch| "aeiou".contains(ch))?;
    /// assert_eq!(value, "Th qck brwn fx jmps vr th lzy dg");
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// For example, to consume all digits while ignoring underscores you could use the following.
    ///
    /// ```rust
    /// # use trivet::parser::parse_from_string;
    /// # use trivet::parser::Parser;
    /// # use trivet::parser::ParseResult;
    /// # fn main() -> ParseResult<()> {
    /// let mut parser = parse_from_string("fe_078A&");
    /// let digits = parser.take_while_unless(|ch| ch.is_ascii_hexdigit(), |ch| ch == '_')?;
    /// assert_eq!(digits, "fe078A");
    /// # Ok(())
    /// # }
    /// ```
    pub fn take_while_unless<T: Fn(char) -> bool, U: Fn(char) -> bool>(
        &mut self,
        include: T,
        exclude: U,
    ) -> ParseResult<String> {
        Ok(self.parser.take_while_unless(include, exclude))
    }

    /// Consume and return characters until an end token is found.
    ///
    /// This uses a greedy version of matching to correctly match
    /// otherwise ambiguous end tokens `"""`.  Consider:
    ///
    /// `"""This thing is "simple""""`
    ///
    /// Here the `take_until("\"\"\"")` does what you would expect,
    /// and the string `"This thing is \"simple\"" can be parsed.
    pub fn take_until(&mut self, token: &str) -> ParseResult<String> {
        Ok(self.parser.take_until(token))
    }

    /// Consume characters while a given test returns true.  This does not save
    /// the characters consumed; if you need that, use `take_while()`.  Iff any
    /// characters are actually consumed, then `true` is returned.
    ///
    /// For example, to discard all digits you might use the following.
    ///
    /// ```rust
    /// # use trivet::parser::parse_from_string;
    /// # use trivet::parser::Parser;
    /// # use trivet::parser::ParseResult;
    /// # fn main() -> ParseResult<()> {
    /// let mut parser = parse_from_string("6748673fred");
    /// let digits = parser.consume_while(|ch| ch.is_ascii_digit())?;
    /// assert!(parser.peek_str("fred")?);
    /// # Ok(())
    /// # }
    /// ```
    pub fn consume_while<T: Fn(char) -> bool>(&mut self, test: T) -> ParseResult<bool> {
        Ok(self.parser.consume_while(test))
    }

    /// Consume characters until an end token is found.  The characters are not returned.
    /// If you need that, consider `take_until`.
    ///
    /// Iff any characters *other than* the end token are consumed, then `true` is
    /// returned.
    ///
    /// This uses a greedy version of matching to correctly match
    /// otherwise ambiguous end tokens `"""`.  Consider:
    ///
    /// `"""This thing is "simple""""`
    ///
    /// Here the `take_until("\"\"\"")` does what you would expect,
    /// and the string `"This thing is \"simple\"" can be parsed.
    pub fn consume_until(&mut self, token: &str) -> ParseResult<bool> {
        let mut retval = false;
        while !self.parser.peek_str_greedy(token) {
            self.parser.consume();
            retval = true;
        }
        self.parser.peek_and_consume_str(token);
        Ok(retval)
    }

    /// Consume all comments.
    ///
    /// This processes both single-line comments and multi-line comments
    /// as defined for the parser instance.  The parser is left at the first
    /// non-whitespace, non-comment character.
    pub fn consume_comments(&mut self) -> ParseResult<bool> {
        // The flag records whether we actually consumed anything on the last
        // trip through the loop.  We stop when the flag remains false.
        let mut flag = false;

        // This flag is set to true if anything is ever consumed at all.
        let mut retval = false;

        // Borrowing here is complicated by the fact that self is borrowed mutably.
        // For this to work, we take the two vectors out of the struct and then put
        // them back when done.
        let single = std::mem::take(&mut self.single_line_comments);
        let multiple = std::mem::take(&mut self.multi_line_comments);
        loop {
            // Consume leading whitespace.  If comments are matched, then trailing
            // whitespace is consumed, so everything should be covered.
            flag |= self.consume_while(|ch| ch.is_whitespace())?;
            // Look for and consume any single-line comments.
            for begin in &single {
                while self.peek_and_consume_str(begin)? {
                    self.take_while(|ch| ch != '\n')?;
                    self.consume_while(|ch| ch.is_whitespace())?;
                    flag = true;
                }
            }
            // Look for and consume any multi-line comments.
            for (begin, end) in &multiple {
                while self.peek_and_consume_str(begin)? {
                    while !self.peek_and_consume_str(end)? {
                        self.consume();
                    }
                    self.consume_while(|ch| ch.is_whitespace())?;
                    flag = true;
                }
            }
            if !flag {
                break;
            } else {
                retval = true;
                flag = false;
            }
        }
        self.single_line_comments = single;
        self.multi_line_comments = multiple;
        Ok(retval)
    }

    /// Parse a Unicode escape.
    ///
    /// Unicode escapes have the form `\u{N}`, where N is up to six hexadecimal
    /// digits.  The `\u` has been consumed before this method is called, so
    /// the parser should be pointing to the opening brace.
    fn parse_unicode_escape(&mut self) -> ParseResult<char> {
        // Get the opening brace.
        if !self.peek_and_consume('{')? {
            // The initial open curly brace is missing.
            let what = match self.peek()? {
                Some(ch) => format!("the character '{}'", ch),
                None => "the end of the stream".to_string(),
            };
            return Err(
                self.unexpected_text_error("the opening brace '{' of a Unicode escape", &what)
            );
        }

        // Get the digits.
        let digits = self.take_while(|ch| ch.is_ascii_hexdigit())?;

        // Get the closing brace.
        if !self.peek_and_consume('}')? {
            // The closing curly brace is missing.
            let what = match self.peek()? {
                None => "the end of the stream".to_string(),
                Some(ch) => format!("the character '{}'", ch),
            };
            return Err(
                self.unexpected_text_error("the closing brace '}' for a Unicode escape", &what)
            );
        }

        // We must have at least one digit.
        if digits.is_empty() {
            // Incorrect Unicode escape.
            return Err(self.syntax_error(
                "Invalid Unicode escape; there must be at least one hexadecimal digit.",
            ));
        }

        // We can have no more than six digits.
        if digits.len() > 6 {
            // Incorrect Unicode escape.
            return Err(self.syntax_error(
                "Unicode escape is too long; it can have a maximum of six hexadecimal digits.",
            ));
        }

        // Convert the Unicode escape to a code point.  We already know the digits
        // are hex and there are at most six of them, so we don't need to check here.
        let value = u32::from_str_radix(&digits, 16).unwrap();
        match char::from_u32(value) {
            Some(ch) => Ok(ch),
            None => Err(self.syntax_error(&format!(
                "The value \\u{{{}}} is not a valid Unicode scalar.",
                digits
            ))),
        }
    }

    /// Parse an escape.
    ///
    /// This assumes the initial backslash has been consumed.
    fn parse_escape(&mut self) -> ParseResult<char> {
        // Process an escape.
        let ch = self.peek()?;
        self.consume();
        match ch {
            None => Err(self.unexpected_eof_error()),
            Some(esc) => match esc {
                'b' => Ok('\x08'),
                'f' => Ok('\x0c'),
                'n' => Ok('\n'),
                'r' => Ok('\r'),
                't' => Ok('\t'),
                'u' | 'U' => Ok(self.parse_unicode_escape()?),
                'x' | 'X' => {
                    let digits = self.peek_n(2)?;
                    self.consume_n(2);
                    match u8::from_str_radix(&digits, 16) {
                        Ok(value) => Ok(value as char),
                        _ => Err(self.syntax_error(&format!(
                            "The value \\u{{{}}} is not a valid Unicode scalar.",
                            digits
                        ))),
                    }
                }
                _ => Ok(esc),
            },
        }
    }

    /// Parse a quoted string.  The parser is assumed to be at the opening
    /// quotation mark on entry.  This is checked and the first instance of
    /// an (unescaped) match terminates the string.  For instance if, on entry,
    /// the parser is pointing at a double quotation mark, then the first
    /// unescaped instance of a double quotation mark ends the string.
    /// This allows this method to be used for both double- and single-quote
    /// strings.
    ///
    /// The following escapes are processed by this method.
    ///
    /// |Escape |Meaning |
    /// |-------|--------|
    /// |`\b`   |Backspace (U+0008) |
    /// |`\f`   |Form Feed (U+000C) |
    /// |`\n`   |Line Feed (U+000A) |
    /// |`\r`   |Carriage Return (U+000D) |
    /// |`\t`   |Tabulation (U+0009) |
    /// |`\u{N}` or `\U{N}` |Unicode Code Point N |
    /// |`\xNN` or `\XNN` |Hexadecimal Escape NN |
    ///
    /// Other escaped characters are taken to be the character itself, so `\\`
    /// is a literal `\`.
    ///
    /// Unicode escapes consist of one to six hexadecimal characters
    /// (case-insensitive).  The result is interpreted as a Unicode
    /// scalar, so values outside the valid range (higher than `0x10ffff`)
    /// will generate an error. The curly braces are required.
    ///
    /// As an example, the following all encode the digit `7`.
    ///
    ///   * `"7"`
    ///   * `"\7"`
    ///   * `"\x37"`
    ///   * `"\u{0037}"`
    ///
    pub fn parse_string(&mut self) -> ParseResult<String> {
        let delimiter = self.peek()?.unwrap_or('"');
        self.consume();
        // Process a simple string that may contain escapes.
        let mut result = String::new();
        while !self.peek_and_consume(delimiter)? {
            if self.peek_and_consume('\\')? {
                // Process an escape.
                result.push(self.parse_escape()?);
            } else {
                match self.peek()? {
                    Some(ch) => {
                        result.push(ch);
                        self.consume();
                    }
                    None => {
                        // String terminated too early.
                        return Err(self.not_found_error(
                            "the closing double quotation mark (\") for a string.",
                        ));
                    }
                }
            }
        }
        self.consume_whitespace()?;
        Ok(result)
    }
}

#[cfg(test)]
#[allow(deprecated)]
mod test {
    use super::parse_from_bytes;
    use super::parse_from_path;
    use super::parse_from_string;
    use super::Loc;
    use super::ParseResult;
    use std::env::temp_dir;
    use std::error::Error;
    use std::fs::File;
    use std::io::Write;
    use std::path::PathBuf;

    #[test]
    fn errors() {
        let parser = parse_from_string("xyzzy");
        let e1 = parser.unexpected_eof_error();
        let e2 = parser.syntax_error("Bad syntax");
        let e3 = parser.unexpected_text_error("a thing", "not the thing");
        let e4 = parser.unexpected_character_error("not a semicolon", ';');
        let e5 = parser.internal_error("Bad things");
        let e6 = parser.not_found_error("the error");
        let e7 = parser.io_error(std::io::Error::from_raw_os_error(22));
        let e8 = parser.unimplemented_error("thing");
        let e8a = parser.unimplemented_error("thing");
        let e9 = parser.error(e8a);
        assert_eq!(
            format!("{}", &e1),
            "<string>:1:1: I/O Error: Unexpected end of file"
        );
        assert_eq!(format!("{}", &e2), "<string>:1:1: Syntax Error: Bad syntax");
        assert_eq!(
            format!("{}", &e3),
            "<string>:1:1: Expected a thing, but found not the thing"
        );
        assert_eq!(
            format!("{}", &e4),
            "<string>:1:1: Expected not a semicolon, but found ';'"
        );
        assert_eq!(
            format!("{}", &e5),
            "<string>:1:1: Internal Error: Bad things"
        );
        assert_eq!(format!("{}", &e6), "<string>:1:1: Could not find the error");
        assert_eq!(
            format!("{}", &e7),
            "<string>:1:1: I/O Error: Invalid argument (os error 22)"
        );
        assert_eq!(format!("{}", &e8), "<string>:1:1: Not Implemented: thing");
        assert_eq!(
            format!("{}", &e9),
            "<string>:1:1: <string>:1:1: Not Implemented: thing"
        );
        assert_eq!(format!("{:?}", &e9.source()), "None");
    }

    #[test]
    fn loc() {
        let internal = Loc::Internal;
        let console = Loc::Console {
            line: 12,
            column: 14,
        };
        let file = Loc::File {
            name: "file.io".to_string(),
            line: 18,
            column: 43,
        };
        assert_eq!(format!("{}", internal), "(internal)");
        assert_eq!(format!("{}", console), "12:14");
        assert_eq!(format!("{}", file), "file.io:18:43");
    }

    #[test]
    fn eof() -> ParseResult<()> {
        let mut parser = parse_from_string("G");
        assert_eq!(parser.loc().to_string(), "<string>:1:1");
        assert!(parser.peek_and_consume('G')?);
        assert_eq!(parser.loc().to_string(), "<string>:1:2");
        // We're at the end of file, but don't know that until we
        // force the parser to look.
        parser.peek()?;
        assert!(parser.is_at_eof());
        Ok(())
    }

    #[test]
    fn peek_and_consume() -> ParseResult<()> {
        let mut parser =
            parse_from_string("the rain \n /*in*/ spain falls\t\t\tmainly/* */ on the plain");
        assert_eq!(parser.loc().to_string(), "<string>:1:1");
        parser.consume();
        assert_eq!(parser.loc().to_string(), "<string>:1:2");
        assert_eq!(parser.peek()?, Some('h'));
        assert!(!parser.peek_and_consume_ws('j')?);
        assert_eq!(parser.peek_n(6)?, "he rai");
        parser.consume_n(6);
        assert_eq!(parser.peek()?, Some('n'));
        parser.consume();
        parser.consume_comments()?;
        assert!(!parser.peek_and_consume_str_ws("spaim")?);
        assert!(!parser.peek_and_consume_str_ws("spain  ")?);
        assert!(parser.peek_and_consume_str_ws("spain")?);
        assert_eq!(parser.loc().to_string(), "<string>:2:15");
        assert!(!parser.peek_and_consume_str("falz")?);
        assert!(parser.peek_and_consume_str("falls")?);
        assert!(parser.consume_whitespace()?);
        assert!(!parser.consume_whitespace()?);
        assert!(!parser.peek_and_consume('n')?);
        assert!(parser.peek_and_consume_str_ws("mainly")?);
        assert!(parser.peek_str("on")?);
        Ok(())
    }

    #[test]
    fn take() -> ParseResult<()> {
        let mut parser = parse_from_bytes(b"any given day:104_218,ab7ac-8ab9--4");
        assert_eq!(
            parser.take_while(|ch| ch.is_alphabetic() || ch.is_whitespace())?,
            "any given day"
        );
        assert!(parser.peek_and_consume(':')?);
        assert_eq!(
            parser.take_while_unless(|ch| ch.is_numeric(), |ch| ch == '_')?,
            "104218"
        );
        assert!(parser.peek_and_consume(',')?);
        assert_eq!(parser.take_until("--")?, "ab7ac-8ab9");
        Ok(())
    }

    #[test]
    fn consume() -> ParseResult<()> {
        let mut parser = parse_from_bytes(b"any given day:104_218,ab7ac-8ab9--4");
        assert!(parser.consume_while(|ch| ch.is_alphabetic() || ch.is_whitespace())?);
        assert!(parser.peek_and_consume(':')?);
        assert!(!parser.consume_while(|ch| ch.is_alphabetic())?);
        assert!(parser.consume_while(|ch| { ch.is_numeric() || ch == '_' })?);
        assert!(parser.peek_and_consume(',')?);
        assert!(parser.consume_until("--")?);
        assert!(parser.peek_str("4")?);
        Ok(())
    }

    #[test]
    fn others() -> ParseResult<()> {
        let mut parser = parse_from_bytes(
            b"any given /* */ /* */ day:104_218,ab7ac-8ab9--4 ->// this is all comment.",
        );
        assert!(parser.peek_and_consume_ws('a')?);
        assert!(parser.peek_and_consume_ws('n')?);
        assert!(parser.peek_and_consume_ws('y')?);
        assert_eq!(parser.peek()?, Some('g'));
        parser.comments_are_whitespace = false;
        assert!(parser.peek_and_consume_str("given")?);
        assert!(parser.consume_whitespace()?);
        assert_eq!(parser.peek()?, Some('/'));
        parser.consume_whitespace_and_comments()?;
        assert_eq!(parser.peek()?, Some('d'));
        assert!(parser.consume_until("->")?);
        assert!(parser.peek_str("//")?);
        assert!(parser.consume_comments()?);

        let mut parser = parse_from_bytes(b"''''This is the way.''''");
        assert!(parser.peek_and_consume_str("'''")?);
        let mut thing = String::new();
        while !parser.peek_str_greedy("'''")? {
            match parser.peek()? {
                Some(ch) => thing.push(ch),
                None => {
                    break;
                }
            }
            parser.consume();
        }
        assert_eq!(thing, "'This is the way.'");
        assert!(parser.peek_and_consume_str("'''")?);

        Ok(())
    }

    #[test]
    fn strings() -> ParseResult<()> {
        let mut parser = parse_from_string("\"\"");
        assert_eq!(parser.parse_string()?, "");
        let mut parser = parse_from_string("\"\\n\\n\"");
        assert_eq!(parser.parse_string()?, "\n\n");
        let mut parser = parse_from_string("\"\\u{be0f}\\u{10448}\"");
        assert_eq!(parser.parse_string()?, "\u{be0f}\u{10448}");
        Ok(())
    }

    #[test]
    fn file() -> ParseResult<()> {
        // Create a file.
        let mut buf = temp_dir();
        buf.push("trivet-test.tmp");
        let mut file = File::create(buf.clone()).expect("Unable to create file");
        write!(
            file,
            r#"
            {{
                "Ḽơᶉëᶆ" : "ȋṕšᶙṁ" ,
                "ḍỡḽǭᵳ" : [ "ʂǐť", "ӓṁệẗ" ] ,
                "Lorem" : {{
                    "ipsum":["dolor","sit","amet"
                }}
            }}
            "#
        )
        .expect("Unable to write to file");

        // Parse the file.
        let mut parser = parse_from_path(&buf)?;
        let thing = parser.take_while_unless(|_| true, |ch| ch.is_whitespace())?;
        assert_eq!(
            thing,
            r#"{"Ḽơᶉëᶆ":"ȋṕšᶙṁ","ḍỡḽǭᵳ":["ʂǐť","ӓṁệẗ"],"Lorem":{"ipsum":["dolor","sit","amet"}}"#
        );

        // Parse a bad file.
        assert!(parse_from_path(&PathBuf::new()).is_err());
        Ok(())
    }

    #[test]
    fn parse_strings() -> ParseResult<()> {
        let mut parser = parse_from_string(
            r#"
            "This is an example\
            \rof a multi\nline string
            that contains \b a lot of
            \f\f\f\f\tescape\ \ \g
            \tcharacters\x2e  \"string\"

            \U{22}\U{22}<-here"
        "#,
        );
        let _ = parser.consume_whitespace()?;
        let result = parser.parse_string()?;
        assert_eq!(
            result,
            format!(
                "{}{}{}",
                "This is an example\n            \rof a multi\nline string\n            ",
                "that contains \u{8} a lot of\n            \u{c}\u{c}\u{c}\u{c}\tescape  ",
                "g\n            \tcharacters.  \"string\"\n\n            \"\"<-here"
            )
            .as_str()
        );
        let mut parser = parse_from_string(r#""\u65e1}""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\u{65e1""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\u{}""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\u{65ke1}""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\u{65ffe1}""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\u{65fffe1}""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\x2g""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\X2e\x2e\U{2e}\u{2e}""#);
        assert_eq!(parser.parse_string()?, "....");
        let mut parser = parse_from_string(r#""\"#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#"""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\U{dcce}""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\U{4fffff}""#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(
            r#"
            // This is a comment.
            // This is another comment.
            /* This is a multi-line
               comment.
            */
            x
        "#,
        );
        parser.consume_whitespace_and_comments()?;
        assert_eq!(parser.peek()?, Some('x'));
        let mut parser = parse_from_string(r#""\U"#);
        assert!(parser.parse_string().is_err());
        let mut parser = parse_from_string(r#""\U{2e"#);
        assert!(parser.parse_string().is_err());
        Ok(())
    }
}
