// Trivet
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/binary-tools/trivet

//! Parse numbers.
//!
//! A general number parser can be found in [`NumberParser`].  This is capable of parsing
//! both integer and floating point numbers, and can do so in decimal, binary, octal, and
//! hexadecimal.

use crate::errors::syntax_error;
use crate::errors::ParseResult;
use crate::Loc;
use crate::ParserCore;
use std::fmt;
use std::fmt::Display;

/// Number radices that are supported.
pub enum Radix {
    Binary,
    Octal,
    Decimal,
    Hexadecimal,
}

impl Radix {
    /// Get the value of each radix.
    pub fn value(&self) -> u32 {
        match self {
            Radix::Binary => 2,
            Radix::Octal => 8,
            Radix::Decimal => 10,
            Radix::Hexadecimal => 16,
        }
    }

    /// Return a closure that can be used to determine if a given character is a legal
    /// digit in the chosen radix.
    pub fn digit_test(&self) -> Box<dyn Fn(char) -> bool> {
        match self {
            Radix::Binary => Box::new(|ch| ch == '0' || ch == '1'),
            Radix::Octal => Box::new(|ch| ('0'..='7').contains(&ch)),
            Radix::Decimal => Box::new(|ch| char::is_ascii_digit(&ch)),
            Radix::Hexadecimal => Box::new(|ch| char::is_ascii_hexdigit(&ch)),
        }
    }
}

impl Display for Radix {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "{}",
            match self {
                Radix::Binary => "Binary",
                Radix::Octal => "Octal",
                Radix::Decimal => "Decimal",
                Radix::Hexadecimal => "Hexadecimal",
            }
        )
    }
}

/// Provide for parsing of numbers in various forms.
///
/// To use this, make an instance and then configure it if you wish.
///
/// # Permitted Radices
///
/// The methods [`Self::parse_u128`], [`Self::parse_i128`], and [`Self::parse_f64`],
/// can parse numbers that occur in any supported radix.  You can disable parsing of
/// numbers in hexadecimal, octal, or binary by setting the appropriate flag to false.
/// Decimal numbers cannot be disabled.
///
/// # Radix Indicator
///
/// Different radices can be detected automatically using a leading radix indicator.
/// This is also configurable, with the following defaults.
///
/// |Prefix |Radix |
/// |-------|------|
/// |`0b`   |Binary |
/// |`0o`   |Octal  |
/// |none   |Decimal |
/// |`0x`   |Hexadecimal |
///
/// Radix indicators can be configured if you wish, but you must be careful to avoid
/// excessive ambiguity.  The following is an example.
///
/// ```rust
/// use trivet::numbers::NumberParser;
/// use trivet::ParserCore;
/// use trivet::errors::ParseResult;
/// use trivet::parse_from_string;
///
/// # fn main() -> ParseResult<()> {
/// let mut parser = parse_from_string("17 $ffef_1021");
/// let mut core = parser.borrow_core();
/// let mut numpar = NumberParser::new();
/// numpar.permit_binary = false;
/// numpar.permit_octal = false;
/// numpar.hexadecimal_indicator = vec!['$'];
///
/// assert_eq!(numpar.parse_u128(&mut core)?, 17);
/// core.consume();
/// assert_eq!(numpar.parse_u128(&mut core)?, 0xffef1021);
/// # Ok(())
/// # }
/// ```
///
/// # Explicit Radix
///
/// Methods that explicitly specify a radix *do not look for* a radix indicator, and
/// will fail if one is encountered.  They also ignore the enabled/disabled status of
/// any radix.
///
/// ```rust
/// use trivet::numbers::NumberParser;
/// use trivet::numbers::Radix;
/// use trivet::ParserCore;
/// use trivet::errors::ParseResult;
/// use trivet::parse_from_string;
///
/// # fn main() -> ParseResult<()> {
/// let mut parser = parse_from_string("17 ffef_1021");
/// let mut core = parser.borrow_core();
/// let mut numpar = NumberParser::new();
///
/// assert_eq!(numpar.parse_u128_radix(&mut core, &Radix::Decimal)?, 17);
/// let _ = core.consume_ws_only();
/// assert_eq!(numpar.parse_u128_radix(&mut core, &Radix::Hexadecimal)?, 0xffef1021);
/// # Ok(())
/// # }
/// ```
///
#[derive(Default, Debug)]
pub struct NumberParser {
    /// Allow underscores in numbers.  If this is true, then arbitrary underscores can
    /// occur in numbers, and these are dropped during parsing.  If false, then
    /// underscores are not permitted.
    pub permit_underscores: bool,

    /// If true, methods that do not explicitly specify a radix will try to detect and
    /// parse hexadecimal numbers.
    pub permit_hexadecimal: bool,

    /// If true, methods that do not explicitly specify a radix will try to detect and
    /// parse octal numbers.
    pub permit_octal: bool,

    /// If true, methods that do not explicitly specify a radix will try to detect and
    /// parse binary numbers.
    pub permit_binary: bool,

    /// Specify the characters that indicate a hexadecimal value.
    pub hexadecimal_indicator: Vec<char>,

    /// Specify the characters that indicate an octal value.
    pub octal_indicator: Vec<char>,

    /// Specify the characters that indicate a binary value.
    pub binary_indicator: Vec<char>,
}

impl NumberParser {
    /// Make a new number parser.  All options are enabled by default, and the
    /// default radix indicators are installed.
    pub fn new() -> Self {
        NumberParser {
            permit_underscores: true,
            permit_hexadecimal: true,
            permit_octal: true,
            permit_binary: true,
            hexadecimal_indicator: vec!['0', 'x'],
            octal_indicator: vec!['0', 'o'],
            binary_indicator: vec!['0', 'b'],
        }
    }

    /// Get the digits (and possibly drop underscores) from the stream based on the specified
    /// radix and return them.
    fn get_digits(&self, parser: &mut ParserCore, radix: &Radix) -> String {
        if self.permit_underscores {
            parser.take_while_unless(radix.digit_test(), |ch| ch == '_')
        } else {
            parser.take_while(radix.digit_test())
        }
    }

    /// Parse an integer in the specified radix.  It is assumed that the radix indicator (if any)
    /// has been parsed, and any minus sign is also parsed (and indicated by `negative`).
    fn parse_i128_body(
        &self,
        parser: &mut ParserCore,
        radix: &Radix,
        negative: bool,
        loc: Loc,
    ) -> ParseResult<i128> {
        // Accumulate digits and (possibly) underscores.
        let mut digits = self.get_digits(parser, radix);

        // Try to convert this into an integer and return it.
        if negative {
            let mut newdigits = String::new();
            newdigits.push('-');
            newdigits.push_str(&digits);
            digits = newdigits;
        }
        match i128::from_str_radix(&digits, radix.value()) {
            Err(err) => {
                // The number was empty or was too big.
                Err(syntax_error(loc, &err.to_string()))
            }
            Ok(value) => Ok(value),
        }
    }

    /// Parse an integer in the specified radix.  It is assumed that the radix indicator (if any)
    /// has been parsed.
    fn parse_u128_body(
        &self,
        parser: &mut ParserCore,
        radix: &Radix,
        loc: Loc,
    ) -> ParseResult<u128> {
        // Accumulate digits and (possibly) underscores.
        let digits = self.get_digits(parser, radix);
        // Try to convert this into an integer and return it.
        match u128::from_str_radix(&digits, radix.value()) {
            Err(err) => {
                // The number was empty or was too big.
                Err(syntax_error(loc, &err.to_string()))
            }
            Ok(value) => Ok(value),
        }
    }

    /// Parse and return a signed integer with the specified radix.  No radix specifier should
    /// be present in the stream.
    pub fn parse_i128_radix(&self, parser: &mut ParserCore, radix: &Radix) -> ParseResult<i128> {
        let loc = parser.loc();
        // Watch for a leading minus sign.
        let negative = parser.peek_and_consume('-');
        self.parse_i128_body(parser, radix, negative, loc)
    }

    /// Parse and return an unsigned integer with the specified radix.  No radix specifier should
    /// be present in the stream.
    pub fn parse_u128_radix(&self, parser: &mut ParserCore, radix: &Radix) -> ParseResult<u128> {
        let loc = parser.loc();
        self.parse_u128_body(parser, radix, loc)
    }

    /// Parse a signed integer from the stream.  The integer's radix is inferred from any
    /// radix prefixes, and only the allowed integer types are checked.
    pub fn parse_i128(&self, parser: &mut ParserCore) -> ParseResult<i128> {
        let loc = parser.loc();

        // Look for a leading minus sign.
        let negative = parser.peek_and_consume('-');

        // Look for a radix indicator in the stream.  If we don't find one, we will assume
        // decimal.  We skip any radix indicators that are either empty or are for a radix
        // that is not allowed.
        if self.permit_hexadecimal
            && !self.hexadecimal_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.hexadecimal_indicator)
        {
            return self.parse_i128_body(parser, &Radix::Hexadecimal, negative, loc);
        }

        if self.permit_octal
            && !self.octal_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.octal_indicator)
        {
            return self.parse_i128_body(parser, &Radix::Octal, negative, loc);
        }

        if self.permit_binary
            && !self.binary_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.binary_indicator)
        {
            return self.parse_i128_body(parser, &Radix::Binary, negative, loc);
        }

        // Found a hexadecimal number.  Parse it.
        self.parse_i128_body(parser, &Radix::Decimal, negative, loc)
    }

    /// Parse an unsigned integer from the stream.  The integer's radix is inferred from any
    /// radix prefixes, and only the allowed integer types are checked.
    pub fn parse_u128(&self, parser: &mut ParserCore) -> ParseResult<u128> {
        let loc = parser.loc();

        // Look for a radix indicator in the stream.  If we don't find one, we will assume
        // decimal.  We skip any radix indicators that are either empty or are for a radix
        // that is not allowed.
        if self.permit_hexadecimal
            && !self.hexadecimal_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.hexadecimal_indicator)
        {
            return self.parse_u128_body(parser, &Radix::Hexadecimal, loc);
        }

        if self.permit_octal
            && !self.octal_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.octal_indicator)
        {
            return self.parse_u128_body(parser, &Radix::Octal, loc);
        }

        if self.permit_binary
            && !self.binary_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.binary_indicator)
        {
            return self.parse_u128_body(parser, &Radix::Binary, loc);
        }

        // Found a hexadecimal number.  Parse it.
        self.parse_u128_body(parser, &Radix::Decimal, loc)
    }

    /// Parse a floating point number from the stream.  It is assumed that the radix indicator (if any)
    /// has been parsed along with any leading minus sign (indicated by `negative`).
    fn parse_f64_body(
        &self,
        parser: &mut ParserCore,
        radix: &Radix,
        negative: bool,
        loc: Loc,
    ) -> ParseResult<f64> {
        // Get the whole part.
        let whole = self.get_digits(parser, radix);

        // Look for a decimal and if found, get the fractional part.
        let fraction = if parser.peek_and_consume('.') {
            self.get_digits(parser, radix)
        } else {
            String::new()
        };

        // Look for an exponent and if found, get the exponent part.
        let has_exponent = match radix {
            Radix::Binary | Radix::Decimal | Radix::Octal => {
                parser.peek_and_consume('e')
                    || parser.peek_and_consume('E')
                    || parser.peek_and_consume('p')
                    || parser.peek_and_consume('P')
            }
            Radix::Hexadecimal => parser.peek_and_consume('p') || parser.peek_and_consume('P'),
        };
        let mut negexp = false;
        let mut exponent = if has_exponent {
            // Look for a sign for the exponent.
            if parser.peek_and_consume('-') {
                negexp = true
            } else {
                let _ = parser.peek_and_consume('+');
            }
            // Get the digits for the exponent.
            self.get_digits(parser, radix)
        } else {
            String::new()
        };

        // Turn the exponent into a number.  It must be an i32 or we can't do anything
        // with it (it will be too large for pow).
        if negexp {
            let mut newdigits = String::new();
            newdigits.push('-');
            newdigits.push_str(&exponent);
            exponent = newdigits;
        }
        let mut expval = if has_exponent {
            match i32::from_str_radix(&exponent, radix.value()) {
                Err(err) => {
                    return Err(syntax_error(
                        loc,
                        &format!("Exponent is not a valid 32-bit signed integer: {}", err),
                    ))
                }
                Ok(value) => value,
            }
        } else {
            0
        };

        // There must be either a whole number or a fraction.  We turn the number into a
        // whole number and then turn that into an integer.  Then we can create a floating
        // point number from that.
        let adjustment = fraction.len() as i32;
        expval -= adjustment;
        let mut mantissa = String::new();
        mantissa.push_str(&whole);
        mantissa.push_str(&fraction);
        if mantissa.is_empty() {
            return Err(syntax_error(
                loc,
                "Number has no whole or fractional digits",
            ));
        }

        // Try to convert the mantissa into a 64-bit integer.  If we can't, remove the least
        // significant digits and increment the exponent.  Note that we truncate, and not round.
        // This is horribly inefficient, but works.
        let manval;
        loop {
            match u64::from_str_radix(&mantissa, radix.value()) {
                Ok(value) => {
                    manval = value;
                    break;
                }
                Err(_) => {
                    mantissa.pop();
                    expval += 1;
                }
            }
        }

        // Okay, now we have a u64, an exponent, and whether the number is negative.
        Ok((if negative { -1.0 } else { 1.0 })
            * (manval as f64)
            * (radix.value() as f64).powf(expval as f64))
    }

    /// Parse a floating point number from the stream.  The number's radix is inferred from any
    /// radix prefixes, and only the allowed integer types are checked.
    ///
    /// Floating point numbers can be in any supported radix, and the radix indicator is checked.
    /// It is it not present, then decimal is assumed.  The structure of a floating point number
    /// is as follows.
    ///
    /// ```text
    /// Float  ::= '-'? ( 'inf' | 'infinity' | 'nan' | Number )
    /// Number ::= ( Digit+ |
    ///              Digit+ '.' Digit* |
    ///              Digit* '.' Digit+ ) Exp?
    /// Exp    ::= ('e'|'p') ('-'|'+') Digit+
    /// Digit  ::= [0-9]
    /// ```
    ///
    /// The exponent can be indicated with either an `e` or a `p`.  If the radix is hexadecimal,
    /// then *only* `p` can be used to avoid ambiguity.
    ///
    /// Some examples.
    ///
    /// |Number   |Value    |
    /// |---------|---------|
    /// |`0,5`    |0.5      |
    /// |`0b0.1`  |0.5      |
    /// |`0b1e-1` |0.5      |
    /// |`0o0.4`  |0.5      |
    /// |`0x0.8`  |0.5      |
    /// |`0x1.4`  |1.25     |
    /// |`0x3p4`  |196,608  |
    /// |`0b0.12e-2` |0.0029296875 |
    ///
    pub fn parse_f64(&self, parser: &mut ParserCore) -> ParseResult<f64> {
        let loc = parser.loc();

        // Look for a leading minus sign.
        let negative = parser.peek_and_consume('-');

        // Look for some special cases.
        if parser.peek_and_consume_chars(&vec!['i', 'n', 'f']) {
            let _ = parser.peek_and_consume_chars(&vec!['i', 'n', 'i', 't', 'y']);
            if negative {
                return Ok(f64::NEG_INFINITY);
            }
            return Ok(f64::INFINITY);
        }
        if parser.peek_and_consume_chars(&vec!['n', 'a', 'n']) {
            return Ok(f64::NAN);
        }

        // Look for a radix indicator in the stream.  If we don't find one, we will assume
        // decimal.  We skip any radix indicators that are either empty or are for a radix
        // that is not allowed.
        if self.permit_hexadecimal
            && !self.hexadecimal_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.hexadecimal_indicator)
        {
            return self.parse_f64_body(parser, &Radix::Hexadecimal, negative, loc);
        }
        if self.permit_octal
            && !self.octal_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.octal_indicator)
        {
            return self.parse_f64_body(parser, &Radix::Octal, negative, loc);
        }
        if self.permit_binary
            && !self.binary_indicator.is_empty()
            && parser.peek_and_consume_chars(&self.binary_indicator)
        {
            return self.parse_f64_body(parser, &Radix::Binary, negative, loc);
        }

        // Found a hexadecimal number.  Parse it.
        self.parse_f64_body(parser, &Radix::Decimal, negative, loc)
    }
}
