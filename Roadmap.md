# Project Roadmap

Planned features will be captured here.

## Roadmap for 2.2.0

- [ ] Consider allowing "unconsume."
- [ ] Consider compiling some patterns.

## Roadmap for 2.1.0

- [ ] Add methods to parse identifiers.
- [ ] Greedy optional match (`peek_and_consume_best([...])`).
- [ ] Break up high-complexity items.  See Lizard output.

## Roadmap for 2.0.0

- [X] Read the entire source in the decoder.
- [X] General performance improvement in decoder.
- [X] Review parser and fold in next byte method.
- [X] Add methods to parse integers.
- [X] Add methods to parse floating point numbers.
- [X] Parser file getting large; refactor pieces.
- [X] Move string parsing into its own module for configurability.

## Roadmap for 1.1.0

- [X] Replicate methods for automatic whitespace collection.
- [X] More tests.

## Roadmap for 1.0.0

- [X] Migrate to stand-alone.

## Wish List

This is a parking lot for things that "would be nice to have."

- [ ] Alternate string parsing methods.
- [ ] i18n, probably via [gettext-rs](https://crates.io/crates/gettext-rs).
- [ ] Improvements to comment handling (permit nesting, handle Lua).
- [ ] Example parser for YAML (whitespace significant).
